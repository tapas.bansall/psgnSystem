var psgnSystem = angular.module('psgnSystem');
psgnSystem.service('entityService', ['$http', '$q', function($http, $q) {

  const entityAPIUrl = "http://172.16.8.248:5555/rest/PSGN_Entity/restServices/";

  this.cache = {};

	this.getEntityData = function (id) {
    this.deferred = $q.defer();
    this.getData(id);
    return this.deferred.promise;
	},

  this.getData = function (id) {
    if (this.cache[id]) {
      this.deferred.resolve(this.cache[id]);
    } else {
      this.getDataFromServer(id);
    }
  },

  this.getDataFromServer = function (id) {
    var _that = this;
    $http.get('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/getEntity?entityId=' + id, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      _that.cache[id] = data;
      _that.deferred.resolve(_that.cache[id]);
    }).error(function (error) {
      _that.deferred.reject(error);
    });
  },

  this.saveCurrentEntity = function (id) {
    this.currentSelection = id;
  },

  this.getCurrentEntity = function () {
    return this.currentSelection;
  }

  this.getAllEntities = function() {
    return $http({
      method: 'get',
      url: entityAPIUrl + "getAllEntities"
    });
  };

  this.getEntity = function(id) {
    return $http({
          method: 'get',
          url: entityAPIUrl + "getEntity?entityId=" + id
      });
  };

}]);
