var psgnSystem = angular.module('psgnSystem');
psgnSystem.service('committeeService', ['$http', '$q', function ($http, $q, ) {

  var committeeAPIUrl = "http://172.16.8.248:5555/rest/PSGN_Board/restServices/"

  this.cache = {};
  this.deferred = null;

  this.getEntityData = function (id) {
    this.deferred = $q.defer();
    this.getData(id);
    return this.deferred.promise;
  },

    this.getData = function (id) {
      if (this.cache[id]) {
        this.deferred.resolve(this.cache[id]);
      } else {
        this.getDataFromServer(id);
      }
    },

    this.getDataFromServer = function (id) {
      var _that = this;
      $http.get('http://localhost:8809/path-to-api?id' + id, {
        headers: { 'Content-Type': 'application/json' },
        transformRequest: angular.identity
      }).success(function (data, status, headers, config) {
        _that.cache[id] = data;
        _that.deferred.resolve(_that.cache[id]);
      }).error(function (error) {
        _that.deferred.reject(error);
      });
    },

    this.saveCurrentCommittee = function (id) {
      this.currentSelection = id;
    },

    this.getCurrentEntity = function () {
      return this.currentSelection;
    },

    this.getAllBoardCommittee = function () {
      return $http({
        method: 'get',
        url: committeeAPIUrl + "getAllBoardCommittee"
      });
      console.log("all board and committee" + data)
    },


    this.createBoardAndCommittee = function (data) {
      return $http({
        method: 'post',
        data: data,
        url: committeeAPIUrl + "createBoardAndCommittee"

      });

    };

  //service call for get the perticular Board And Committee starts
 
  this.saveBoardAndCommittee = function (id) {
    this.currentSelectionCommitee = id;

  };


  this.getCurrentboardAndcommittee = function () {
    
    return this.currentSelectionCommitee;
  }


  this.getCurrentboardAndccommitteedata = function (id) {


    this.getboardandcommiteDataFromServer(id);

  }


  this.getboardandcommiteDataFromServer = function (id) {


    return $http({
      method: 'get',
      url: committeeAPIUrl + "getBoardAndCommittee?board_Committee_id=" + id


    });

  }


  // getting perticular board and committee ends//

  // get particular task to view start

  this.viewcurrenttask = function(id){
    this.currentselectedtaskid = id   // assigning selected id to variable t

  }
  this.getselectedtask = function(){
    return this.currentselectedtaskid  //retuning the selected task id to retrive from committee controller
  }
  this.gettaskdataFromServer = function(data){
    
    return $http({
      method: 'get',
      url: committeeAPIUrl + "reviewRequest?requestBoardCommiteeId="+data.boardcomittrrId + '&taskID='+ data.taskId
      

    });

  }



  this.modifyBoardAndCommittee = function (data) {
    return $http({
      method: 'post',
      data: data,
      url: committeeAPIUrl + "createBoardAndCommittee"
    });
  };

  this.renewBoardAndCommittee = function (data) {
    return $http({
      method: 'post',
      data: data,
      url: committeeAPIUrl + "renewBoardAndCommittee"
    });
  }

  this.updateTask = function(data){
    return $http({
      method:'post',
      data :data,
      url:committeeAPIUrl + "updateTask"
    })
  }

}]);
