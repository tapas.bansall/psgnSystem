var psgnSystem = angular.module('psgnSystem');
psgnSystem.service('memberService', ['$http', '$q', function($http, $q) {

  var memberApiUrl = "http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/";

  this.cache = {};

  this.deferred = null;

  this.getMemberData = function(id) {
    this.deferred = $q.defer();
    this.getData(id);
    return this.deferred.promise;
  },

  this.getData = function(id) {
    if (this.cache[id]) {
      this.deferred.resolve(this.cache[id]);
    } else {
      this.getDataFromServer(id);
    }
  },

  this.getDataFromServer = function(id) {
    var _that = this;
    $http.get('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/getDatabankMember?userId=' + id, {
      headers: {
        'Content-Type': 'application/json'
      },
      transformRequest: angular.identity
    }).success(function(data, status, headers, config) {
      _that.cache[id] = data;
      _that.deferred.resolve(_that.cache[id]);
    }).error(function(error) {
      _that.deferred.reject(error);
    });
  },

  this.saveCurrentMember = function(id) {
    this.currentSelection = id;
  },

  this.getCurrentMember = function() {
    return this.currentSelection;
  }

  this.convertFile = function(documentsToSend) {
    this.deferred = $q.defer();
    this.getBuffer(documentsToSend);
    return this.deferred.promise;
  },


  this.getBuffer = function(fileToSend) {
    if (!FileReader.prototype.readAsBinaryString) {
      FileReader.prototype.readAsBinaryString = function(fileData) {
        var binary = "";
        var pt = this;
        var reader = new FileReader();
        reader.onload = function(e) {
          var bytes = new Uint8Array(reader.result);
          var length = bytes.byteLength;
          for (var i = 0; i < length; i++) {
            binary += String.fromCharCode(bytes[i]);
          }
          //pt.result  - readonly so assign binary
          pt.content = binary;
          $(pt).trigger('onload');
        }
        reader.readAsArrayBuffer(fileData);
      }
    }
    var count = 0;
    var fileArray = [];
    fileArray = $.extend(fileArray, fileToSend);
    if (fileToSend.length === 0) {
      this.deferred.resolve([]);
    }
    for (var key = 0; key < fileToSend.length; key++) {
      if (fileToSend[key]['type'] === 'File') {
        var _that = this;
        (function(index) {
          var reader = new FileReader();
          if (fileToSend[index].file[0] == undefined) {
            _that.deferred.resolve(fileArray);
          } else {
            fileArray[index].fileName = fileToSend[index].file[0].name;
            reader.readAsBinaryString(fileToSend[index].file[0]);
            reader.onload = function() {
              count++;
              if (reader.result) {
                var bytes = btoa(reader.result);
              } else if (reader.content) {
                var bytes = reader.content;
              } else {
                var bytes = '';
              }
              fileArray[index].bytes = bytes;
              if (count === fileToSend.length) {
                _that.deferred.resolve(fileArray);
              }
            }
          }
        })(key);
      } else {
        count++;
        if (count === fileToSend.length) {
          this.deferred.resolve(fileArray);
        }
      }
    }
  };

  this.createMember = function(data) {
      return $http({
          method: 'post',
          url: memberApiUrl + "createDatabankMember",
          //headers: { 'Content-Type': undefined },
          data: data
      });
  };

  this.updateMember = function(data) {
      return $http({
          method: 'post',
          url: memberApiUrl + "modifyDatabankMember",
          //headers: { 'Content-Type': undefined },
          data: data
      });
  };

  this.getRoles = function() {
    return $http({
          method: 'get',
          url: memberApiUrl + "getRoles",
          //headers: { 'Content-Type': undefined }
      });
  }

  this.getCountries = function() {
    return $http({
          method: 'get',
          url: memberApiUrl + "getCountries",
          //headers: { 'Content-Type': undefined }
      });
  }

  this.getMembers= function() {
      return $http({
          method: 'get',
          url: memberApiUrl + "getDataBankMembers",
          //headers: { 'Content-Type': undefined }
      });
  }

  this.addAdhocMember = function(data){
    return $http({
          method: 'post',
          url: memberApiUrl + "createAdHocMember",
          //headers: { 'Content-Type': undefined },
          data: data
      });
  }


}]);