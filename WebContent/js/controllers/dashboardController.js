var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('dashboardController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    // Dashboard Entities
    $scope.enableEntity = false;
    $scope.enableBoard = false;
    $scope.enableMembers = false;
    $scope.enableReport = false;
    $scope.enableTrainingAndSupport = false;
    $scope.showThreeIcons = false;
    $rootScope.actionPerformedIsCreate = false;
    $rootScope.userIsCoordinator = false;
    $rootScope.userIsSLCCoordinator = false;
    $rootScope.userIsApprover = false;
    $rootScope.userIsHH = false;
    $rootScope.userIsTECLeadership = false;
    $rootScope.userIsChair = false;
    $rootScope.userIsDatabankMember = false;
    $rootScope.userIsFadCoordinator = false;
    $rootScope.userIsSecurityCoordinator = false;
    $rootScope.userIsDofCoordinator = false;
    $rootScope.userIsBoardSecretary = false;
    $scope.clickedOnCommitte = false;
    $scope.clickedOnMember = false;

    $scope.iconsCount = 0;
    $scope.findUserRole();
    $scope.findIconsCountAndAlign();
    $scope.getActionListData();
    $scope.getNotifications();
  };

  $scope.findUserRole = function() {
    if ($rootScope.loggedUser.role === 'TEC Coordinator') {
      $scope.showIconsInDashboard(true, true, true, true, true);
      $scope.iconsCountRowOne = 3;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsCoordinator = true;

    } 
    // else if($rootScope.loggedUser.role ==='GE Coordinator'){
    //   $scope.showIconsInDashboard(true, true, true, true, true);
    //   $scope.iconsCountRowOne = 3;
    //   $scope.iconsCountRowTwo = 2;
    //   $rootScope.userIsGECoordinator = true;

    // }
    else if($rootScope.loggedUser.role === 'SLC Coordinator'){
      $scope.showIconsInDashboard(true, true, false, true, true);
      $scope.iconsCountRowOne = 2;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsSLCCoordinator = true;
    } else if($rootScope.loggedUser.role === 'TEC Approver'){
      $scope.showIconsInDashboard(true, true, true, true, true);
      $scope.iconsCountRowOne = 3;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsApprover = true;
    } else if($rootScope.loggedUser.role === 'HH'){
      $scope.showIconsInDashboard(false, false, true, true, true);
      $scope.iconsCountRowOne = 1;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsHH = true;
    } else if($rootScope.loggedUser.role === 'TEC Leadership'){
      $scope.showIconsInDashboard(false, false, true, true, true);
      $scope.iconsCountRowOne = 1;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsTECLeadership = true;
    } else if($rootScope.loggedUser.role === 'Board_Chair'){
      $scope.showIconsInDashboard(false, true, true, true, true);
      $scope.iconsCountRowOne = 2;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsChair = true;
    } else if($rootScope.loggedUser.role === 'Databank Member'){
      $scope.showIconsInDashboard(false, false, true, false, false);
      $scope.iconsCountRowOne = 1;
      $scope.iconsCountRowTwo = 1;
      $rootScope.userIsDatabankMember = true;
    } else if($rootScope.loggedUser.role === 'Board Secretary'){
      $scope.showIconsInDashboard(false, false, true, false, false);
      $scope.iconsCountRowOne = 1;
      $scope.iconsCountRowTwo = 1;
      $rootScope.userIsBoardSecretary = true;
    } else if($rootScope.loggedUser.role === 'DOF Coordinator'){
      $scope.showIconsInDashboard(false, false, false, true, true);
      $scope.iconsCountRowOne = 3;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsDofCoordinator = true;
    } else if($rootScope.loggedUser.role === 'FAD Coordinator'){
      $scope.showIconsInDashboard(false, false, false, true, true);
      $scope.iconsCountRowOne = 3;
      $scope.iconsCountRowTwo = 2;
      $rootScope.userIsFadCoordinator = true;
    } else if($rootScope.loggedUser.role === 'Security Coordinator') {
      $scope.showIconsInDashboard(false, true, true, false, false);
      $scope.iconsCountRowOne = 2;
      $scope.iconsCountRowTwo = 1;
      $rootScope.userIsSecurityCoordinator = true;
    }
  };

  $scope.showIconsInDashboard = function(showEntity, showBoard, showMembers, showReport, showTraining){
      $scope.enableEntity = showEntity;
      $scope.enableBoard = showBoard;
      $scope.enableMembers = showMembers;
      $scope.enableReport = showReport;
      $scope.enableTrainingAndSupport = showTraining;
  };

  $scope.findIconsCountAndAlign = function(){
      switch($scope.iconsCountRowOne) {
        case 1:
          $scope.dashboardIconColsRowOne = "col-md-12";
          $scope.iconTextPositionInRowOne = "firstRowIconMarginWhenOneInRow";
          break;
        case 2:
          $scope.dashboardIconColsRowOne = "col-md-6";
          $scope.iconTextPositionInRowOne = "firstRowIconMarginWhenMultipleInRow";
          break;
        case 3:
          $scope.dashboardIconColsRowOne = "col-md-4";
          $scope.iconTextPositionInRowOne = "firstRowIconMarginWhenMultipleInRow";
          break;
      }
      switch($scope.iconsCountRowTwo) {
        case 1:
          $scope.dashboardIconColsRowTwo = "col-md-12";
          $scope.iconPosition = "iconPositionWhenOneInRow";
          $scope.iconTextPosition = "iconTextWhenOneInRow";
          break;
        case 2:
          $scope.dashboardIconColsRowTwo = "col-md-6";
          $scope.iconPosition = "iconPositionWhenTwoInRow";
          $scope.iconTextPosition = "iconTextWhenTwoInRow";
          break;
      }
  };

  $scope.checkRequestPermission = function () {
  	if ($rootScope.loggedUser.role === 'TEC Coordinator' || $rootScope.loggedUser.role === 'GE Coordinator') {
  		return true;
  	}
  	return false;
  };

  $scope.checkReviewPermission = function () {
  	if ($rootScope.loggedUser.role === 'GE Coordinator') {
  		return false;
  	}
  	return true;
  };

  $scope.checkNotificationPermission = function () {
  	if ($rootScope.loggedUser.role === 'TEC Coordinator' || $rootScope.loggedUser.role === 'TEC Approver') {
  		return true;
  	}
  	return false;
  };

  $scope.checkSectorLead = function () {
  	if ($rootScope.loggedUser.role === 'TEC Sector Lead') {
  		return false;
  	}
  	return true;
  };

  $scope.showGovtEntity = function(){
    var user = $rootScope.loggedUser;

    if(user.role == "TEC Coordinator"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.deactivate = false;
        $scope.modalIconsClass = "col-md-6";
        $scope.renew = false;
        $scope.evaluate = false;
        $scope.dataMember = false;
    } else if(user.role == "TEC Approver"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.deactivate = false;
        $scope.modalIconsClass = "col-md-6";
        $scope.renew = false;
        $scope.evaluate = false;
        $scope.dataMember = false;
    } else if(user.role == "SLC Coordinator"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.deactivate = false;
        $scope.modalIconsClass = "col-md-6";
        $scope.renew = false;
        $scope.evaluate = false;
        $scope.dataMember = false;
    } else if(user.role == "HH"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.deactivate = false;
        $scope.modalIconsClass = "col-md-6";
        $scope.renew = false;
        $scope.evaluate = false;
        $scope.dataMember = false;
    } else {
        $scope.navigateTo('requestList')
    }
    $rootScope.govtBoard = "createRequest";
    $scope.viewCurrent = "viewEntities";
    $scope.modifyCurrent = "requestList";
  };

  $scope.showGovtCommitee = function(){
    var user = $rootScope.loggedUser;

    if(user.role == "TEC Coordinator" || user.role == "Board_Chair"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.modalIconsClass = "col-md-6";
        $scope.dataMember = false;
    } else if(user.role == "TEC Approver"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.modalIconsClass = "col-md-6";
        $scope.dataMember = false;
    } else if(user.role == "SLC Coordinator" || user.role == "Security Coordinator"){
        $scope.create = false;
        $scope.update = true;
        $scope.modify = true;
        $scope.modalIconsClass = "col-md-6";
        $scope.dataMember = false;
    }
    $rootScope.govtBoard = "createCommitee";
    $scope.viewCurrent = "viewCommittee";
    $scope.clickedOnCommitte = true;
    $scope.modifyBordCommitte = "statusOfRequestsBordCommitte";
    $scope.viewBoardCommittes = "viewCommittee";
  };
  
  

  $scope.showDataMembers = function(){
    $scope.create = false;
    $scope.update = false;
    $scope.modify = true;
    $scope.modalIconsClass = "col-md-12";
    $scope.dataMember = true;
    $scope.showThreeIcons = true;
    $rootScope.govtBoard = "createMember";
    $scope.viewCurrent = "viewDatabankMembers";
    $scope.modifyCurrent = "requestMemberList";
    $scope.clickedOnMember = true;
    $scope.navigateTo('viewDatabankMembers');
  };

  $scope.showPublishCommunication = function () {
    $scope.navigateTo('publishCommunication');
  };

  $scope.showTrainingMaterial = function () {
    $scope.navigateTo('needMoreSupport');
  };

  $scope.showAllRequests = function(){
    $('#myModal').modal('hide');
    setTimeout(function(){
      if($scope.clickedOnCommitte == false) {
        $scope.navigateTo($scope.modifyCurrent);
      }
      else {
        $scope.navigateTo($scope.modifyBordCommitte);
      }
    }, 500);
  }

  $scope.showAllEntities = function(){
    $('#myModal').modal('hide');
    setTimeout(function(){
      $scope.navigateTo($scope.viewCurrent);
    }, 500);
  }

  $scope.showAllDeactivateRequests = function(){
    $('#myModal').modal('hide');
    setTimeout(function(){
      $scope.navigateTo('requestList');
    }, 500);
  }

  $scope.actionList = function(){
    $scope.navigateTo('actionList');
  }

  $scope.getActionListData = function () {
	  $http.get('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/getMyAction?userId=' + $rootScope.loggedUser.userId, {
       headers: { 'Content-Type': 'application/json'},
       transformRequest: angular.identity
     }).success(function (data, status, headers, config) {
       $scope.actionListData = data.myAction;
       if($("#actionListReload").hasClass('loading')) {
            $('#actionListReload').removeClass('loading');
        }
     }).error(function (error) {
       console.log(error);
        if($("#actionListReload").hasClass('loading')) {
            $('#actionListReload').removeClass('loading');
        }
     });
  };
  
  $scope.reloadActionList = function () {
	  $('#actionListReload').addClass('loading');
	    $scope.getActionListData();
  };

	  $scope.reloadNotifications = function () {
		    $('#notificationsReload').addClass('loading');
		    $scope.getNotifications();
		  };

		  $scope.getNotifications = function () {
		     $http.get('http://172.16.8.248:5555/rest/TEC_PublishCommunication/restServices/getPublishCommunication?userId=' + $rootScope.loggedUser.userId, {
		       headers: { 'Content-Type': 'application/json'},
		       transformRequest: angular.identity
		     }).success(function (data, status, headers, config) {
		       $scope.notifications = data;
		       if($("#notificationsReload").hasClass('loading')) {
		            $('#notificationsReload').removeClass('loading');
		        }
		     }).error(function (error) {
		       console.log(error);
		        if($("#notificationsReload").hasClass('loading')) {
		            $('#notificationsReload').removeClass('loading');
		        }
		     });
		  };


  $scope.viewActionListData = function(listInput){
    switch(listInput.type){
      case "dataBankMembers":
        $rootScope.viewMemberClick(listInput.taskId);
        break;

      case "GovernmentEntities":
        $rootScope.viewEntityClick(listInput.taskId);
        break; 

      case "boardCommitte":
        $rootScope.viewCommitteeClick(listInput.taskId);
        break;
    }
  };

  $scope.showReport = function(){
    window.open('http://172.16.8.244:8080/presto/hub/dashboard/dashboard.jsp?guid=9f84e8d6-9924-4b87-a211-52f2a7d85ef1', '_blank');
  };

  $scope.init();
}]);
