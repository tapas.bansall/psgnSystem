var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('commiteeController', ['$scope', '$rootScope', '$http', '$timeout', 'memberService', 'committeeService', function($scope, $rootScope, $http, $timeout, memberService, committeeService) {
  var selectedValuesCommitte = [];


  // $rootScope.data = $rootScope.viewdata
 
  $scope.init = function() {
    $scope.dontShow = true;
    $scope.disableCheckbox = false;
    $scope.disabledAddMemberButton = false;
    $scope.memberCount = "";
    $scope.searchData = "";
    $scope.warningText = true;
    $scope.editClicked = false;
    $scope.disableButton = false;
    $scope.flagForTable = false;
    $scope.memberExisted = false;
    $scope.membersOfBoard = new Array();
    
    $scope.setMode();
    $scope.viewdata = {};
    $scope.searchData = {
      searchText: ''
    }
    $scope.data = {};
    $scope.tempStoreMemberData = {};
    $scope.memberInfoData = {};
    $scope.summaryInformation= [];
    $scope.governingLegislation = []
    $scope.committeeSummaryInfo = [];
    $scope.governingLegislation = [];
    // $scope.data.committeeSummaryInfo.termStartDate = new Date(dateString)
    // $scope.data.committeeSummaryInfo.termEndDate = new Date(dateString)
    console.log($scope.committeeSummaryInfo, 'asd')
    $scope.rowCount = 1;
    var userDetails = window.localStorage.getItem('userDetails');
    $scope.loggedUser = JSON.parse(userDetails);
    $scope.regex = '^(\\+971)(2|3|4|6|7|9|50|55)\\d{7}$';
    $scope.uploadedDocuments = [];
    $scope.SLCDocuments = [];
    //$scope.nonGovtAppointments = [];
    $scope.structureBoardCommittee = [];
    for (var i = 0; i < $scope.rowCount; i++) {
      $scope.addRow();
    }

    //Show TEC Coordinator Inputs
    $scope.freeText = false;
  };

// committee modes
  $scope.setMode = function() {
    switch($rootScope.committeeMode){
      case "view":
        $scope.formSubmittedSuccessfully = false;
        $scope.hideSuccessMessage = false;
        $scope.viewCommitteeMode();
        break;

      case "create":
        $scope.formSubmittedSuccessfully = false;
        break;

      case "modify":
        $scope.viewCommitteeMode();
        $scope.formSubmittedSuccessfully = false;
        break;

        case false :
        $scope.viewtaskmode();
        break;
    }
  };



  $scope.onSubmit = function(evt) {
    $rootScope.loading = true;
    var promise = memberService.convertFile($scope.uploadedDocuments).then(function(fileArray) {
      if (($rootScope.committeeMode != undefined) && ($rootScope.committeeMode == 'create')) {
        $scope.data.requestType = 'Create';
      } else if(($rootScope.committeeMode != undefined) && ($rootScope.committeeMode == 'modify')) {
        $scope.data.requestType = 'Modify';
        $scope.data.boardCommitteeId = $rootScope.radioEnabledEntity.boardCommitteeId;
      }
      $scope.data.status = 'submitted';
      $scope.data.userId = $rootScope.loggedUser.userId;
      var data = $scope.data;
      data.summaryInformation = $scope.summaryInformation;
      data.members = $scope.membersOfBoard;
      data.uploadedDocuments = fileArray;
      data.governingLegislation = $scope.governingLegislation;
      data.governanceFunctions = $scope.governanceFunctions;
      $rootScope.dataToSendToMembers = $scope.data.committeeSummaryInfo;
      if ($scope.structureBoardCommittee != undefined) {
        $rootScope.positionValue = $scope.structureBoardCommittee[0].title;
      }
      data = JSON.stringify(data);
      console.log(data);
      if($scope.data.requestType == 'Create'){
        committeeService.createBoardAndCommittee(data).then(function(data) {
          $rootScope.loading = false;
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
          $scope.error = false;
          $('html,body').animate({
            scrollTop: $('.alert-success').offset().top - 200
          }, 'slow');

          $scope.getRequests()

        }, function(error) {
          $scope.error = true;
          $rootScope.loading = false;
          $('html,body').animate({
            scrollTop: $('.alert-danger').offset().top - 200
          }, 'slow');
        });
      }else{
        committeeService.createBoardAndCommittee(data).then(function(data) {
          $rootScope.loading = false;
          $scope.formModifiedSuccessfully = true;
          $scope.hideButtons = true;
          $scope.error = false;
          $('html,body').animate({
            scrollTop: $('.alert-success').offset().top - 200
          }, 'slow');
          $scope.getRequests()
        }, function(error) {
          $scope.error = true;
          $rootScope.loading = false;
          $('html,body').animate({
            scrollTop: $('.alert-danger').offset().top - 200
          }, 'slow');
        });
      }
      
      
    });
  };


  $scope.uploadDocuments = function() {
    angular.element("#uploadDoc").trigger('click');
  };

  $scope.addRow = function() {
    $scope.structureBoardCommittee.push({
      memberNameAR: '',
      memberNameEN: '',
      title: '',
      typeOfAppointment: '',
      appointmentAuthority: '',
      totalAsLegislation: '',
      totalAsCommittee: ''
    });
  };

  $scope.formSubmitted = function() {
    $scope.isFormSubmitted = true;
  };

  $scope.deleteRow = function(index, record) {
    $scope.finalListOfMembers.splice(index, 1);
    var findInTablePosition = $scope.members.indexOf(record);
    $scope.members[findInTablePosition].selectedMember = false;
  }

  $scope.editRow = function(index, record) {
    $scope.tempStoreMemberData = {};
    $scope.tableIndex = index;
    $scope.editClicked = true;
    $scope.dontShow = false;
    $scope.flagForTable = false;
    $scope.memberInfoData = record;
    Object.assign($scope.tempStoreMemberData, record);
    console.log($scope.tempStoreMemberData);
  }

  $scope.updateMember = function(record) {
    $scope.flagForTable = true;
    //$scope.finalListOfMembers.splice(index, 1);
    var findInTablePosition = $scope.members.indexOf(record);
    $scope.members[findInTablePosition].selectedMember = false;
  }

  $scope.cancelUpdateMember = function(index, record) {
    //var findInTablePosition = $scope.finalListOfMembers.indexOf(record);
    $scope.finalListOfMembers[$scope.tableIndex] = $scope.tempStoreMemberData;
    //$scope.finalListOfMembers =
    $scope.flagForTable = true;
  }

  $scope.getSelectedValueCommitte = function(selectedValuesCommitte) {
    angular.forEach(selectedValuesCommitte, function(index) {
      if (selectedValuesCommitte.indexOf('Other') >= 0) {
        $scope.flagOtherCommitte = true;
      } else {
        $scope.flagOtherCommitte = false;
      }
    });
  }
  $scope.goPreviousPage = function() {
    window.history.back();
  }

  //File methods
  $scope.promisifyFile = function(fileType) {
    $scope.documentFrom = fileType;
    var promise = new Promise($scope.getBuffer);
    promise.then(function(success) {
      $scope.tempDocument.file[0] = success;
    });
  }

  $scope.promisifySLCFile = function(fileType) {
    $scope.documentFrom = fileType;
    var promise = new Promise($scope.getSLCBuffer);
    promise.then(function(success) {
      $scope.SLCDocument.file[0] = success;
    });
  };

  $scope.getBuffer = function(resolve) {
    var reader = new FileReader();
    reader.readAsArrayBuffer($scope.tempDocument.file[0]);
    reader.onload = function() {
      var arrayBuffer = reader.result
      var bytes = new Uint8Array(arrayBuffer);
      resolve(bytes);
    }
  };

  $scope.getSLCBuffer = function(resolve) {
    var reader = new FileReader();
    reader.readAsArrayBuffer($scope.SLCDocument.file[0]);
    reader.onload = function() {
      var arrayBuffer = reader.result
      var bytes = new Uint8Array(arrayBuffer);
      resolve(bytes);
    }
  }

  $scope.openFileInput = function(id) {
    if (typeof id === 'number') {
      angular.element('#input-' + id).trigger('click');
    } else {
      $timeout(function() {
        angular.element('#' + id).trigger('click');
      });
    }
  };

  $scope.tempDocument = {
    name: '',
    type: 'Link',
    file: '',
    comment: ''
  }

  $scope.SLCDocument = {
    name: '',
    type: 'Link',
    file: '',
    comment: ''
  }

  $scope.onDocumentUploadSave = function(fileSource) {
    console.log(fileSource, "asd");
    if (fileSource == "SLCDocuments") {
      $scope.SLCDocuments.push($scope.SLCDocument);
      console.log("SLC", $scope.SLCDocuments)
    } else {
      $scope.uploadedDocuments.push($scope.tempDocument);
      console.log("sup", $scope.uploadedDocuments)

    }
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    }
  };


  $scope.deleteFile = function(name) {
    console.log($scope.uploadedDocuments);
    $scope.uploadedDocuments[name] = null;
  };


  $scope.deleteFileFromUpload = function(index, documentFrom) {
    if (documentFrom == "uploadedDocuments") {
      $scope.uploadedDocuments.splice(index, 1);
    } else {
      $scope.SLCDocuments.splice(index, 1);
    }
  };

  $scope.saveRequest = function() {
    $scope.data.active = 1;
    $scope.data.requestType = "create";
    $scope.data.userId = $rootScope.loggedUser.userId;
    $scope.data.uploadedDocuments = $scope.uploadedDocuments;
    $scope.data.SLCDocuments = $scope.SLCDocuments;
    $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
    if ($rootScope.userIsCoordinator) {
      $scope.data.status = "draft";
    } else if ($rootScope.userIsSLCCoordinator) {
      $scope.data.status = "hhApproved";
    }
    var data = JSON.stringify($scope.data);
    $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
    $http.post('http://localhost:8080/Entity', data, {
      headers: {
        'Content-Type': 'application/json'
      },
      transformRequest: angular.identity
    }).success(function(data, status, headers, config) {
      console.log('data posted');
      $scope.formSubmittedSuccessfully = true;
      $scope.hideButtons = true;
    }).error(function(error) {
      console.log(error);
    });
  }

  $scope.clearSecurity = function(member) {
    var rejectedUsers = 0;
    //call update databank member api and send security cleared
    //may need recursion fn instead of forloop below
    for (i = 0; i < $scope.structureBoardCommittee.length; i++) {
      if ($scope.structureBoardCommittee[i].userStatus != "Approved") {
        rejectedUsers++;
      }
    }
    if (rejectedUsers != 0) {
      $rootScope.buttonStatus(true, false, false, false, false, false, false, true, true, false, false);
    } else {
      $rootScope.buttonStatus(true, false, false, false, false, true, false, false, true, false, false);
    }
  };

  $scope.showAdhocMember = function() {
    $scope.memberInfoData = {};
  };

  $scope.saveAdhocMember = function() {
    //$scope.members.push($scope.memberInfoData);
    //$scope.showAdhocMember();
    $rootScope.loading = true;
    memberService.addAdhocMember($scope.memberInfoData).then(function(data) {
      $('#addAdhoc').modal('hide');
      $scope.getNoOfMembers($scope.memberCount);
      $('#documentModal').modal('show');
      $rootScope.loading = false;
    }, function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.getNoOfMembers = function(count) {
    //$scope.count = count;
    $scope.disableSelection();
    $rootScope.loading = true;
    $http.get('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/getDataBankMembers', {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.members = data.res;
      if ($scope.membersOfBoard.length > 0) {
        //$scope.membersOfBoard selected members
        //$scope.members all databank members
        var index = 0;
        var getAllPositions = [];

        function loopThruSelectedMembers() {
          if ($scope.membersOfBoard.length > index) {
            var counter = 0;

            function loopThruDBMembers() {
              if ($scope.members.length > counter) {
                if ($scope.members[counter].UserId == $scope.membersOfBoard[index].UserId) {
                  getAllPositions.push(counter);

                  index++;
                  loopThruSelectedMembers();
                } else {

                  counter++;
                  loopThruDBMembers();
                }
              } else {
                index++;
                loopThruSelectedMembers();
              }
            }
            loopThruDBMembers();
          } else {
            for (var i = 0; i < $scope.members.length; i++) {
              if ($scope.memberCount == getAllPositions.length) {
                $scope.members[i].disableCheckbox = true;
              } else {
                $scope.members[i].disableCheckbox = false;
              }
            }
            for (var k = 0; k < getAllPositions.length; k++) {
              $scope.members[getAllPositions[k]].selectedMember = true;
              $scope.members[getAllPositions[k]].disableCheckbox = false;
            }
            $rootScope.loading = false;
            $scope.memberCount = count;
            $scope.flagForTable = false;
            $scope.dontShow = true;
          }
        }
        loopThruSelectedMembers();
      } else {
        $rootScope.loading = false;
        $scope.memberCount = count;
        $scope.flagForTable = false;
        $scope.dontShow = true;
      }
    }, function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.disableSelection = function() {
  };


  $scope.memberSelected = function(record) {
    console.log(record, "record")
    //push this record inside a new array
    //find this record in the array and then and make its selected value true
    //count the no of true values, if it is more than selectedCount then disable the other fields
    if ($scope.membersOfBoard.length > 0) {
      var position = $scope.membersOfBoard.indexOf(record);
      //var result = $scope.membersOfBoard.filter(function(boardMember) { boardMember.UserId == record.UserId});
      $scope.memberExisted = false;
      for (var y = 0; y < $scope.membersOfBoard.length; y++) {
        if ($scope.membersOfBoard[y].UserId == record.UserId) {
          $scope.membersOfBoard.splice(position, 1);
          $scope.memberExisted = true;
        }
      }
      if ($scope.memberExisted == false) {
        $scope.membersOfBoard.push(record);
      }
    } else {
      $scope.membersOfBoard.push(record);
    }


    if ($scope.memberCount != $scope.membersOfBoard.length) {
      $scope.warningText = false;
      $scope.disableCheckbox = false;
      for (i in $scope.members) {
        $scope.members[i].disableCheckbox = false;
      }
    } else {
      $scope.warningText = false;
      for (i in $scope.members) {
        if ($scope.members[i].selectedMember) {
          $scope.members[i].disableCheckbox = false;
        } else {
          $scope.members[i].disableCheckbox = true;
        }
      }
    }
  };

  $scope.sendAdhocMembersToForm = function() {
    $scope.flagForTable = true;
    if ($scope.flagForTable == true) {
      $scope.finalListOfMembers = $scope.membersOfBoard;
    }
  };

  $scope.showTable = function() {
    $scope.flagForTable = true;
  };


// view perticular biard and committree//
// $scope.test="test"
$scope.viewCommitteeMode = function () {
  
  
  var id = committeeService.getCurrentboardAndcommittee();
  $rootScope.loading = true;
  committeeService.getboardandcommiteDataFromServer(id).then(function(data) {

    console.log("response from view all BC",data)
    debugger;
    $scope.data = data.data;

    
    // $scope.data.summaryInformation = $scope.viewdata.committeeSummaryInfo
    // $scope.data.governingLegislation = $scope.viewdata.governingLegislation
    // $scope.data.committeeSummaryInfo = $scope.viewdata.committeeSummaryInfo
    // $scope.governingLegislation = $scope.data.boardCommitteeStructure
    $rootScope.loading = false;
    console.log("view data", $scope.data.committeeSummaryInfo.termStartDate)
  }, function(error) {
    $rootScope.loading = false;
  });
};

// view particular task..
$scope.viewtaskmode = function(){
var id = committeeService.getselectedtask();
$rootScope.loading = true;
committeeService.gettaskdataFromServer(id).then(function(data) {

  console.log("response from view all BC",data)
  debugger;
  $scope.data = data.data;
  
  // $scope.data.summaryInformation = $scope.viewdata.committeeSummaryInfo
  // $scope.data.governingLegislation = $scope.viewdata.governingLegislation
  // $scope.data.committeeSummaryInfo = $scope.viewdata.committeeSummaryInfo
  // $scope.governingLegislation = $scope.data.boardCommitteeStructure
  $rootScope.loading = false;
  console.log(" task view data"+ $scope.data)
}, function(error) {
  $rootScope.loading = false;
});

}



  $scope.filterByMemberName = function(request) {
    if ($scope.searchData && $scope.searchData.searchText && $scope.searchData.searchText.length > 0) {
      if (!request.nameAR) {
        var memberAR = "";
      } else {
        var memberAR = request.nameAR.toLowerCase();
      }
      if (!request.nameEN) {
        var memberEN = "";
      } else {
        var memberEN = request.nameEN.toLowerCase();
      }
      if (!request.EmailAddress) {
        var EmailAddress = "";
      } else {
        var EmailAddress = request.EmailAddress.toLowerCase();
      }
      var filter = $scope.searchData.searchText.toLowerCase();
      return ((memberEN.indexOf(filter) !== -1) || (memberAR.indexOf(filter) !== -1) || (EmailAddress.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };

  $scope.getRequests = function() {

    $rootScope.loading = true;
    committeeService.getAllBoardCommittee().success(function(data) {
      $scope.data = data;
      console.log("response from get all board and committe api "+$scope.data)
      $rootScope.loading = false;
      $scope.dataOfEntity = $.extend(true, [], $scope.data);
      // $scope.getRows();
      // $scope.getColumns();
      // $scope.selectedForDeactivation = {};
    }).error(function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.init();
}]);