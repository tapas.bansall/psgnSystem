var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('notificationController', ['$scope', '$rootScope', '$http', '$translate', '$timeout', function ($scope, $rootScope, $http, $translate, $timeout) {
  $scope.init = function () {
    $scope.notification = {
      template: 'template1'
    };
    $scope.editable = false;
    $scope.getAllExistingTemplates();
    $scope.getAllNotifications();
  };

  $scope.getAllExistingTemplates = function () {
    $http.get('http://172.16.8.248:5555/rest/TEC_PublishCommunication/restServices/getUserGroup', {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.notificationResponse = data;
      if ($scope.notificationResponse.template && $scope.notificationResponse.template.length > 0) {
        $scope.notification.template = "0";
        $scope.notification.subject = $scope.notificationResponse.template[0].subject;
        $scope.notification.message = $scope.notificationResponse.template[0].message;
        $scope.notification.id = $scope.notificationResponse.template[0].id;
        $scope.notification.newTemplate = false;
      } else {
        $scope.notification.newTemplate = true;
      }
      window.setTimeout(function() {
	      $translate(['SLCT_ALL','ALL_SELECT']).then(function(translations) {
	        $('.multiselect-ui').multiselect({
	          includeSelectAllOption: true,
	          selectAllText: translations.SLCT_ALL,
	          allSelectedText: translations.ALL_SELECT
	        });
	      });
	    }, 2000);
    }).error(function (error) {
      $scope.notificationResponse = {
        user_group: [],
        template: []
      }
    });
  };

  $scope.getAllNotifications = function () {
    $http.get('http://172.16.8.248:5555/rest/TEC_PublishCommunication/restServices/getPublishCommunication?userId=' + $rootScope.loggedUser.userId, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (error) {
      $scope.data = [];
    });
  };

  $scope.editTemplate = function () {
    $scope.editable = true;
  };

  $scope.disableTemplate = function () {
    $scope.editable = false;
  };

  $scope.loadNewTemplate = function () {
    if ($scope.notification.template !== 'addNew') {
      var tmp = $scope.notificationResponse.template[$scope.notification.template];
      $scope.notification.subject = tmp.subject;
      $scope.notification.message = tmp.message;
      $scope.notification.id = tmp.id;
      $scope.notification.newTemplate = false;
      $scope.editable = false;
    } else {
      $scope.notification.subject = '';
      $scope.notification.message = '';
      $scope.notification.newTemplate = true;
      $scope.notification.id = null;
      $scope.editable = true;
    }
  };

  $scope.sendNotification = function () {
    if (!$rootScope.loading) {
      $rootScope.loading = true;
      var data = JSON.stringify($scope.notification);
      $http.post('http://172.16.8.248:5555/rest/TEC_PublishCommunication/restServices/postPublishCommunication', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
      }).success(function (data, status, headers, config) {
        $('#notificationModal').modal('show');
        $rootScope.loading = false;
      }).error(function (error) {
        $scope.response = {
          status: "error",
        };
        $timeout(function(){
          $('html,body').animate({
            scrollTop: $('#sendNotificationFailed').offset().top - 200
          }, 'slow');
        });
        $rootScope.loading = false;
      });
    }
  };

  $scope.init();
}]);