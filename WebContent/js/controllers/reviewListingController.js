var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('reviewListingController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $scope.pages = [];
    $scope.limit = 5;
    $scope.getRequests();
  };

  $scope.getRequests = function () {
    $rootScope.loading = true;
    $http.get('http://172.16.8.248:5555/rest/PSGN_Board/restServices/reviewRequest?userId=' + $rootScope.loggedUser.userId, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.records = data.Output;
      $rootScope.loading = false;
    }).error(function (error) {
      $rootScope.loading = false;
    });
  };

  $scope.openRequest = function () {
    if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'TEC Coordinator') {
      window.location.hash = '/viewRequest';
    } else if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'TEC Approver') {
      window.location.hash = '/approveRequest';
    } else {
      window.location.hash = '/viewRequest';
    }
  };

  $scope.init();
}]);