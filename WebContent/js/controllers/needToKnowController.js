var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('needToKnowController', ['$scope', '$rootScope', '$http', '$translate', function ($scope, $rootScope, $http, $translate) {
  $scope.init = function () {

    $scope.notification = {
      template: 'template1'
    };
    $scope.editable = false;
    $scope.getAllExistingTemplates();
    $scope.getAllNotifications();
  };

  $scope.getAllExistingTemplates = function () {
    // var data = JSON.stringify($scope.notification);
    // $http.get('http://172.16.8.248:5555/rest/restServices/selectCUR?userId' + $rootScope.loggedUser.userId, {
    //   headers: { 'Content-Type': 'application/json'},
    //   transformRequest: angular.identity
    // }).success(function (data, status, headers, config) {
    //   $scope.data = data;
    // }).error(function (error) {
    //   console.log(error);
    // });
    $scope.getDummyTemplates();
  };

  $scope.getAllNotifications = function () {
  	$http.get('http://172.16.8.248:5555/rest/TEC_PublishCommunication/restServices/getPublishCommunication?userId=' + $rootScope.loggedUser.userId, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.notification.template = 0;
    }).error(function (error) {
      console.log(error);
    });
    
    //$scope.getDummyData();

  };

  $scope.getDummyData = function () {
    $scope.data = [{
      subject: 'Hamdan bin Mohammed reviews Dubai Municipality plan for Emirati housing projects.',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      date: '15/11/2017'
    }, {
      subject: 'Maktoum bin Mohammed heads first Strategic Affairs Council meeting.',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      date: '23/11/2017'
    }, {
      subject: 'Hamdan bin Mohammed invites public to vote for best government initiative.',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      date: '23/11/2017'
    }, {
      subject: 'Mohammed bin Rashid chairs Dubai Executive Council meeting.',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      date: '20/11/2017'
    }, {
      subject: 'His Highness directs DHA to reduce waiting periods at outpatient clinics.',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      date: '18/11/2017'
    }];
  };

  $scope.getDummyTemplates = function () {
    $scope.templates = [{
      subject: 'template1',
      message: 'You are suspended',
      id: 1
    },{
      subject: 'template2',
      message: 'Something happended to send nitification',
      id: 2
    }, {
      subject: 'template3',
      message: 'Yogi has been promoted to tech lead for all new projects.',
      id: 3
    }, {
      subject: 'template4',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
      id: 4
    }, {
      subject: 'template5',
      message: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
      id: 5
    }];
    $scope.notification.template = "0";
    $scope.notification.subject = $scope.templates[0].subject;
    $scope.notification.message = $scope.templates[0].message;
    $scope.notification.id = $scope.templates[0].id;
  };

  $scope.editTemplate = function () {
    $scope.editable = true;
  };

  $scope.disableTemplate = function () {
    $scope.editable = false;
  };

  $scope.loadNewTemplate = function () {
    if ($scope.notification.template !== 'addNew') {
      var tmp = $scope.templates[$scope.notification.template];
      $scope.notification.subject = tmp.subject;
      $scope.notification.message = tmp.message;
      $scope.notification.id = tmp.id;
      $scope.editable = false;
    } else {
      $scope.notification.subject = '';
      $scope.notification.message = '';
      $scope.notification.id = null;
      $scope.editable = true;
    }
  };

  $scope.sendNotification = function () {
    var data = JSON.stringify($scope.notification);
    $http.post('http://172.16.8.248:5555/rest/restServices/selectCUR', data, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.init();
}]);
