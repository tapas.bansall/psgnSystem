var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('mainController', ['$scope', '$rootScope',  '$http', '$translate', 'memberService', 'entityService', function ($scope, $rootScope, $http, $translate, memberService, entityService) {
  $scope.init = function () {
    //nothing
    $rootScope.showButtonBasedOnRadio = false;
    $scope.showViewButton = false;
    $scope.showModifyButton = false;
    $scope.showDeactivateButton = false;
    $scope.menuDisplay = false;
    $rootScope.application = {
      language : 'ar'
    };
    $scope.system = window.system;
    var lang = window.localStorage.getItem('appLanguage');
    if (lang) {
      $translate.use(lang);
      $rootScope.application.language = lang;
    }
  };

  // $rootScope.viewCommitteeClick = function (id) {
  //   $rootScope.disableFields = true;
  //   $rootScope.hhApprovalCommentsDisabled = true;
  //   $rootScope.accordianStatus(false);  
  //   $rootScope.coordinatorCommentsDisabled = false;
  //   $rootScope.buttonStatus(true, false, false, false, false, false, false, false, false, false, false);
  //   $rootScope.formDisabled = true;
  //   $scope.navigateTo('createCommitee');
  // };

  $rootScope.goToShowButtons = function(data) {
    $rootScope.radioEnabledEntity = data;
      $scope.showViewButton = true;
      $scope.showModifyButton = true;
      $scope.showDeactivateButton = true;
      $rootScope.showButtonBasedOnRadio = true;
  };

  $rootScope.showCreateRequest = function() { 
    $rootScope.actionPerformedIsCreate = true;
    $rootScope.disableFields = false;
    $rootScope.hhApprovalCommentsDisabled = true;
    $rootScope.coordinatorCommentsDisabled = false;
    $rootScope.buttonStatus(true, true, false, true, false, false, false, false, true, false, false);
    $rootScope.accordianStatus(false);
    $('#myModal').modal('hide');
    setTimeout(function(){
      switch($scope.govtBoard) {
        case 'createRequest': $rootScope.entityMode = 'create'; break;
        case 'createMember': $rootScope.memberMode = 'create'; break;
        case 'createCommitee': $rootScope.committeeMode = 'create'; break;
      }
      if($scope.govtBoard == undefined){
        $scope.govtBoard = 'createMember'
      }
      $scope.navigateTo($scope.govtBoard);
    }, 500);
  }

  $scope.getTemplatePath = function (templateName, subfolder) {
    if (subfolder) {
      return $scope.system + 'components/' + subfolder + '/' + templateName + '.html';
    }
    return $scope.system + 'components/' + templateName + '.html';
  };

  $scope.navigateTo = function (location) {
  	window.location.hash = '/' + location;
  };

  $scope.logout = function () {
  	$rootScope.loggedUser = null;
    $rootScope.userLogged = false;
    window.localStorage.removeItem('userDetails');
    $scope.navigateTo('');
    document.location.reload(); 
  };

  $scope.displayMenu = function () {
    $scope.menuDisplay = true;
  };
  $scope.changePassword = function(){
    $scope.navigateTo('changePassword');
  }

  $scope.hideMenu = function () {
    $scope.menuDisplay = false;
  };

  $scope.switchLanguage = function (key) {
    $translate.use(key);
    window.localStorage.setItem('appLanguage', key);
    location.reload();
  };

  $scope.scrollToView = function(id){
    if (id) {
      window.setTimeout(function() {
        $('html,body').animate({
          scrollTop: $('#'+id).offset().top - 200
        }, 'slow');
      },500);
    }
  };

//   $scope.sendEmail = function(email, subject, body) {
//     // var email = 's.mahesha88@gmail.com',
//     var link = "mailto:"+ 's.mahesha88@gmail.com'
//              + "?subject=Change Password request " + escape(subject)
//              + "&body= please chage your password using below link" + escape(body); 

//     window.location.href = link;
//  };

  $scope.init();
}]);