var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('loginController', ['$scope', '$translate', '$rootScope', '$http', function($scope, $translate, $rootScope, $http) {
  $scope.init = function() {
    angular.element('body').addClass('login-body');
    $scope.data = {
      language: 'ar',
      username: '',
      password: ''
    };

    $scope.checkLogin();

    $scope.displayError = false;

    $scope.defaultCredentials = [{
        name: 'Sami Mohammed Ali',
        role: 'TEC Coordinator',
        password: 'sami@123',
        username: 'sami@tec.gov.ae',
        department: 'Governance Department',
        userId: 16
      },
      {
        name: 'Hamdah Bin Kalban',
        role: 'TEC Approver',
        password: 'hamdah@123',
        username: 'hamdah@tec.gov.ae',
        department: 'Governance Department',
        userId: 17
      },
      {
        name: 'Mohammed Aletawi',
        role: 'SLC Coordinator',
        password: 'mohammad@123',
        username: 'Mohammed.AlEtawi@slc.dubai.gov.ae',
        department: 'Governance Department',
        userId: 20
      },
      {
        name: 'Hala Salem',
        role: 'Sector Lead',
        password: 'hala@123',
        username: 'Hala.salem@tec.gov.ae',
        department: 'Governance Department',
        userId: 21
      },
      {
        name: 'Ayman Nadim',
        role: 'DGHR Coordinator',
        password: 'ayman@123',
        username: 'Ayman.Nadim@tec.gov.ae',
        department: 'Governance Department',
        userId: 18
      },
      {
        name: 'Qais Shawwa',
        role: 'DOF Coordinator',
        password: 'qais@123',
        username: 'Qais.shawwa@tec.gov.ae',
        department: 'Governance Department',
        userId: 19
      }, {
        name: 'R Mohammed',
        role: 'HH',
        password: 'mohammed@123',
        username: 'r.mohammed@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 24
      }, {
        name: 'TEC Leadership',
        role: 'TEC Leadership',
        password: 'mohammed@123',
        username: 'a.mohammed@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 25
      }, {
        name: 'Board_Chair',
        role: 'Board_Chair',
        password: 'mohammed@123',
        username: 'k.mohammed@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 26
      }, {
        name: 'Databank Member',
        role: 'Databank Member',
        password: 'mohammed@123',
        username: 'y.mohammed@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 27
      }, {
        name: 'Board Secretary',
        role: 'Board Secretary',
        password: 'secretary@123',
        username: 'secretary@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 28
      }, {
        name: 'FAD Coordinator',
        role: 'FAD Coordinator',
        password: 'fadcoordinator@123',
        username: 'fadcoordinator@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 29
      }, {
        name: 'Security Coordinator',
        role: 'Security Coordinator',
        password: 'securityCoordinator@123',
        username: 'securityCoordinator@dubai.gov.ae',
        department: 'Governance Department Coordinator',
        userId: 30
      }
    ];
    $scope.view = 'login';
  };

  $scope.onSubmit = function() {
    $rootScope.loading = true;
    //$scope.data.password = btoa($scope.data.password);
    var data = JSON.stringify($scope.data);
    var url = 'http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/login';
    url += '?password=' + $scope.data.password;
    url += '&userName=' + $scope.data.username;
    $http.get(url, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $rootScope.loggedUser = {
        name: data.personalInformation.nameEN,
        role: data.role,
        username: data.personalInformation.emailAddress,
        department: 'Governance Department Coordinator',
        userId: data.userId
      };
      console.log("logged in user role"+$rootScope.loggedUser.role)
      window.localStorage.setItem('userDetails', JSON.stringify($rootScope.loggedUser));
      $rootScope.userLogged = true;
      $scope.displayError = false;
      $scope.navigateTo('dashboard');
      angular.element('body').removeClass('login-body');
      $rootScope.loading = false;
    }).error(function (error) {
      $scope.displaySuccess = false;
      $scope.displayError = true;
      $rootScope.loading = false;
    });
  };

  $scope.checkLogin = function() {
    var tmp = window.localStorage.getItem('userDetails');
    if (tmp) {
      tmp = JSON.parse(tmp);
      $rootScope.loggedUser = tmp;
      $rootScope.userLogged = true;
      $scope.navigateTo('dashboard');
      angular.element('body').removeClass('login-body');
    }
  };

  $scope.changeLanguage = function(key) {
    $translate.use(key);
  };

  $scope.resetPassword = function() {
    $('#resetPasswordModal').modal('show');
  };

  $scope.switchView = function (view) {
    $scope.view = view;
  };

  $scope.init();
}]);