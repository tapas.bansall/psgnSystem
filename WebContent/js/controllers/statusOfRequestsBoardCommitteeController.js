var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('statusOfRequestsBoardCommitteeController', ['$scope', '$rootScope', '$http','committeeService', function($scope, $rootScope, $http,committeeService) {
  $scope.init = function() {
    $scope.isSelected = false
    $scope.getRequests();
    $rootScope.viewMode = false;
    $rootScope.chairCommentsDisabled = true;
    $scope.taskdata=[]
  };

  $scope.getRequests = function() {
    $rootScope.loading = true;
    $http.get('http://172.16.8.248:5555/rest/PSGN_Board/restServices/getReviewTasks?userId=' + $rootScope.loggedUser.userId, {
      headers: {
        'Content-Type': 'application/json'
      },
      transformRequest: angular.identity
    }).success(function(data, status, headers, config) {
      $scope.records = data.Output;
      console.log("status of request",$scope.records)
      $rootScope.loading = false;
      $scope.statusMapping();
    }).error(function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.statusMapping = function() {
    if ($rootScope.loggedUser.role == "TEC Coordinator" || $rootScope.loggedUser.role == "Board_Chair") {
      for (var i = 0; i < $scope.records.length; i++) {
        switch ($scope.records[i].status) {
          case "draft":
            $scope.records[i].status = "Draft";
            break;
          case "submitted":
            if ($scope.records[i].typeOfRequest != "Evaluate") {
              $scope.records[i].status = "Submitted";
              break;
            } else {
              $scope.records[i].status = "Pending";
              break;
            }
          case "rejected":
            if ($scope.records[i].typeOfRequest != "Evaluate") {
              $scope.records[i].status = "To be Modified";
              break;
            } else {
              $scope.records[i].status = "Pending";
              break;
            }
          case "complete":
            $scope.records[i].status = "To be Completed";
            break;
          case "requestNotFinalized":
            $scope.records[i].status = "To be Modified";
            break;
        }
      }
    } else if ($rootScope.loggedUser.role == "TEC Approver") {
      for (var i = 0; i < $scope.records.length; i++) {
        switch ($scope.records[i].status) {
          case "finalized":
            $scope.records[i].status = "Awaiting Final Approval";
            break;
          case "submitted":
            if ($scope.records[i].typeOfRequest != "Evaluate") {
              $scope.records[i].status = "Pending";
              break;
            } else {
              $scope.records[i].status = "Evaluate Pending Request";
              break;
            }
        }
      }
    } else if ($rootScope.loggedUser.role == "SLC Coordinator") {
      for (var i = 0; i < $scope.records.length; i++) {
        switch ($scope.records[i].status) {
          case "sentForHHApproval":
            $scope.records[i].status = "Awaiting HH Approval";
            break;
        }
      }
    } else if ($rootScope.loggedUser.role == "Security Coordinator") {
      for (var i = 0; i < $scope.records.length; i++) {
        switch ($scope.records[i].status) {
          case "approved":
            $scope.records[i].status = "Pending";
            break;
        }
      }
    }
  }

  //Onclick of review
  $scope.viewRequest = function(record) {
    committeeService.viewcurrenttask($rootScope.reqdata);
    // $rootScope.memberMode = 'view';
    $rootScope.committeeMode = false;
    $scope.navigateTo('createCommitee');
    //Disable or enable fieldset
    //show or hide buttons(exit, save, submit, approve, reject)
    console.log("slectd taskid",$rootScope.taskId)
    console.log("record status",record.taskStatus)
    if (record.taskStatus == 'Draft' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = false;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.accordianStatus(false);
      $rootScope.coordinatorCommentsDisabled = false;
      $rootScope.buttonStatus(true, true, false, true, false, false, false, false, true, false, false);
    } else if (record.taskStatus == 'Submitted' && ($rootScope.loggedUser.role == "TEC Coordinator" || $rootScope.loggedUser.role == "Board_Chair")) {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.accordianStatus(false);
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, false, false);
    } else if (record.taskStatus == 'To be Completed' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.accordianStatus(true);
      $rootScope.buttonStatus(true, false, false, false, false, false, true, false, true, true, false);
    } else if (record.taskStatus == 'To be Modified' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.accordianStatus(false);
      $rootScope.buttonStatus(true, true, true, true, false, false, false, false, true, true, false);
    } else if (record.taskStatus == 'Submitted' && $rootScope.loggedUser.role == "TEC Approver") {
      $rootScope.disableFields = true;
      $rootScope.approverCommentsDisabled = false;
      $rootScope.accordianStatus(false);
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, true, true, false, false, true, true, false);
    } else if (record.taskStatus == 'Awaiting Final Approval' && $rootScope.loggedUser.role == "TEC Approver") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.accordianStatus(false);
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, true, false, false, false, true, true, false);
    } else if (record.taskStatus == 'Awaiting HH Approval' && $rootScope.loggedUser.role == "SLC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.buttonStatus(true, false, false, false, true, false, false, false, true, true, false);
      $rootScope.accordianStatus(true);
    } else if (record.taskStatus == 'Draft' && $rootScope.loggedUser.role == "Board_Chair") {
      $rootScope.disableFields = false;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.accordianStatus(false);
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, true, false, true, false, false, false, false, true, false, false);
    } else if (record.taskStatus == 'Pending' && $rootScope.loggedUser.role == "Security Coordinator") {
      $rootScope.disableFields = false;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.accordianStatus(false);
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, true, false, false, true, false, false);
    } else if (record.taskStatus == 'Pending' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.accordianStatus(false);
      $rootScope.chairCommentsDisabled = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, true, false, true, false, false, false, false, true);
    } else if (record.taskStatus == 'Evaluate Pending Request' && $rootScope.loggedUser.role == "TEC Approver") {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.accordianStatus(false);
      $rootScope.chairCommentsDisabled = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, true, true, false, false, false, false, true);
    }
  }

  // $rootScope.buttonStatus = function(exit, save, del, submit, approve, reject, complete, coordinatorCommentSection, approverCommentSection, chairCommentSection){
  $scope.viewCommitteeRequest = function(record) {
    $rootScope.viewMode = true;
    if (record.taskStatus == 'Draft' && ($rootScope.loggedUser.role == "TEC Coordinator" || $rootScope.loggedUser.role == "Board_Chair")) {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, false, false);
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.accordianStatus(false);
    } else if (record.taskStatus == 'Submitted' && ($rootScope.loggedUser.role == "TEC Coordinator" || $rootScope.loggedUser.role == "Board_Chair")) {
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, false, false);
      $rootScope.accordianStatus(false);
    } else if (record.taskStatus == 'To be Completed' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, true, false);
      $rootScope.accordianStatus(true);
    } else if (record.taskStatus == 'To be Modified' && $rootScope.loggedUser.role == "TEC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, true, false);
      $rootScope.accordianStatus(false);
    } else if (record.taskStatus == 'Submitted' && $rootScope.loggedUser.role == "TEC Approver") {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, false, false);
      $rootScope.accordianStatus(false);
    } else if (record.taskStatus == 'Awaiting Final Approval' && $rootScope.loggedUser.role == "TEC Approver") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, true, false);
      $rootScope.accordianStatus(false);
    } else if (record.taskStatus == 'Awaiting HH Approval' && $rootScope.loggedUser.role == "SLC Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.hhApprovalCommentsDisabled = false;
      $rootScope.approverCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, false, true, true, false);
      $rootScope.accordianStatus(true);
    } else if (record.taskStatus == 'Pending' && $rootScope.loggedUser.role == "Security Coordinator") {
      $rootScope.disableFields = true;
      $rootScope.hhApprovalCommentsDisabled = true;
      $rootScope.accordianStatus(false);
      $rootScope.coordinatorCommentsDisabled = true;
      $rootScope.buttonStatus(true, false, false, false, false, false, false, true, true, false, false);
    }
    $scope.navigateTo('createCommitee');
  }

  $scope.allApproved = function() {
    $rootScope.buttonStatus(true, false, false, false, false, false, false, true, true, false);
  }

  $scope.goToViewCommittee = function() {
    $scope.navigateTo('viewCommittee');
  }

  $scope.goToSelectedrow = function(data){
      $rootScope.taskId = data;
      $rootScope.reqdata = {
        "taskId" : data.taskID,
        "boardcomittrrId" :data.requestBoardCommiteeId
      }
      console.log("selected task is",$rootScope.reqdata)
      $scope.isSelected = true
      $scope.showViewButton = true;
      $scope.showModifyButton = true;
      $scope.showDeactivateButton = true;
      $rootScope.showButtonBasedOnRadio = true;
  }
  // update request task..
  $scope.update = function(){
    var data = $scope.taskdata
    committeeService.updateTask(data)
  }
$scope.modifiyTask = function(){
  
}
  $scope.init();
}]);