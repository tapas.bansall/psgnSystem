
var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('reviewController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {

    $scope.data = {
      boardInclusion: '',
      govtEntityName: '',
      entityName: '',
      mandate: '',
      sector: '',
      typology: '',
      appointedDirectorGeneral: '',
      officialGazetteUrl: '',
      establishedLegislationReference: '',
      affiliation: '',
      remarks: ''
    };
    $scope.rowCount = 4;
    var userDetails = window.localStorage.getItem('userDetails');
    $scope.loggedUser = JSON.parse(userDetails);
    $scope.lastTab = false;
    $scope.regex = '^(\\+971)(2|3|4|6|7|9|50|55)\\d{7}$';
    $scope.uploadedDocuments = {};
    //$scope.summaryInformation = [];
    $scope.summaryInformation = {
        legislationRequired : 'yes'
    };
    $scope.governanceFunctions = [];
    $scope.governingLegislation = [];
    for (var i=0;i<$scope.rowCount;i++) {
      $scope.addRow();
    }
    $scope.isFormCompleted = false;
    $scope.isFormApproved = false;
    $scope.isFormRejected = false;
    $scope.forApprover = false;
    $scope.forCoordinator = false;
    $scope.forSlcCoordinator = false;
    $scope.checkRoleAndShowButtons();

  };

  $scope.onSubmit = function (evt) {
    var data = $scope.data;
    data.summaryInformation = $scope.summaryInformation;
    data.governingLegislation = $scope.governingLegislation;
    data.governanceFunctions = $scope.governanceFunctions;
    data = JSON.stringify(data);
    $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
    if($scope.isFormCompleted == true){
        $http.post('http://172.16.8.248:5555/rest/restServices/cur', data, {
          headers: { 'Content-Type': 'application/json'},
          transformRequest: angular.identity
        }).success(function (data, status, headers, config) {
          console.log('data posted');
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
        }).error(function (error) {
          console.log(error);
        });  
    } else if($scope.isFormApproved == true) {
        $http.post('http://172.16.8.248:5555/rest/restServices/cur', data, {
          headers: { 'Content-Type': 'application/json'},
          transformRequest: angular.identity
        }).success(function (data, status, headers, config) {
          console.log('data posted');
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
        }).error(function (error) {
          console.log(error);
        });  
    } else if($scope.isFormRejected == true){
        $http.post('http://172.16.8.248:5555/rest/restServices/cur', data, {
          headers: { 'Content-Type': 'application/json'},
          transformRequest: angular.identity
        }).success(function (data, status, headers, config) {
          console.log('data posted');
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
        }).error(function (error) {
          console.log(error);
        });
    } else {
        $http.post('http://172.16.8.248:5555/rest/restServices/cur', data, {
          headers: { 'Content-Type': 'application/json'},
          transformRequest: angular.identity
        }).success(function (data, status, headers, config) {
          console.log('data posted');
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
        }).error(function (error) {
          console.log(error);
        });
    }
  };

  $scope.deleteFile = function (name) {
    console.log($scope.uploadedDocuments);
    $scope.uploadedDocuments[name] = null;
  };

  $scope.uploadDocuments = function() {
    angular.element("#uploadDoc").trigger('click');
  };

  $scope.addRow = function () {
    $scope.governanceFunctions.push({
      brandednameAR: '',
      brandednameEN: '',
      planningPerformanceManagement: '',
      funding: '',
      humanResource: '',
      fadAudit: '',
      internalAudit: '',
      externalAudit: '',
      procurementLaw: '',
      itStandards: '',
      communicationstandards: ''
    });
    $scope.governingLegislation.push({
      brandednameAR: '',
      brandednameEN: '',
      establishmentType: '',
      establishmentLegistlationReferenceNumber: '',
      establishmentLegistlationReferenceYear: '',
      establishmentLegistlationURL: '',
      fundingSource1: '',
      fundingSource2: '',
      fundingSource3: '',
      fundingSourceOther: '',
      accountingApproach: '',
      accountingApproachOther: ''
    });
  };

  $scope.formSubmitted = function () {
    $scope.isFormSubmitted = true;
    console.log($scope.createRequest.$valid)
  };

  $scope.formCompleted = function () {
    $scope.isFormCompleted = true;
    $scope.isFormSubmitted = true;
    console.log($scope.createRequest.$valid)
  };

  $scope.formAppoved = function() {
    $scope.isFormApproved = true;
    $scope.isFormSubmitted = true;

  }

  $scope.formRejected = function() {
    $scope.isFormRejected = true;
    $scope.isFormSubmitted = true;
  }

  $scope.checkRoleAndShowButtons = function() {
      if($rootScope.loggedUser.role == "TEC Approver"){
          $scope.accessToApprover = true;
          $scope.forApprover = true;
      } else if($rootScope.loggedUser.role == "TEC Coordinator"){
          $scope.accessToApprover = true;
          $scope.forCoordinator = true;
      } else if($rootScope.loggedUser.role == "SLC Coordinator"){
          $scope.accessToApprover = true;
          $scope.forSlcCoordinator = true;
      }
  }

  $scope.init();
}]);
