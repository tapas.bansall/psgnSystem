var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('needMoreSupportController', ['$scope', '$rootScope', '$http', '$translate', function ($scope, $rootScope, $http, $translate) {
  $scope.init = function () {
    $scope.getTrainingMaterials();
  };
  $scope.tabChanged = function (evt) {
    $('.navbar li, .tabtitle').removeClass('active');
    var newid = $(evt.target).parent().attr('id');
    newid=newid.replace('tab-', '');
    $(evt.target).parent().addClass('active');
    $('.tabcontent').hide();
    $('#tabcon-' + newid).fadeIn();
  };


  $scope.getTrainingMaterials = function(){
    $translate(['OD_TRNG_MAT','PSGN_TRNG_MTRL','ARIS_ARCHTCT','ARIS_DSIGNR']).then(function(translations) {
      $scope.data = [{
        title: translations.OD_TRNG_MAT,
        url: "http://www.google.com",
        image: window.system + "images/OD Training Material.png"
      },{
        title: translations.PSGN_TRNG_MTRL,
        url: "http://www.google.com",
        image: window.system + "images/PSGN Training Material.png"
      },{
        title: translations.ARIS_ARCHTCT,
        url: "http://www.google.com",
        image: window.system + "images/ARIS_Architect.png"
      },{
        title: translations.ARIS_DSIGNR,
        url: "http://www.google.com",
        image: window.system + "images/ARIS Designer.png"
      }];
    });
  };

  $scope.openPdfFile = function(filelink){
    window.open(filelink);
  }

  // $scope.getStarted= function(){
  //   window.open('http://google.com');
  // }

  $scope.init();
}]);