var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('memberController', ['$scope', '$rootScope', '$http', 'memberService', '$timeout', function($scope, $rootScope, $http, memberService, $timeout) {
  $scope.flagOtherMember  = false;
  $scope.flagOtherSkill = false;
  $scope.flagOtherGovtAppoint  = false;
  $scope.init = function() {
    $scope.getRoles();
    $scope.getCountries();
    $scope.skillsList();
    $scope.documentsToSendToUpload = [];
    if ($rootScope.dataToSendToMembers != undefined) {
      $scope.dubaiGovtAppointments = [];
      $scope.dubaiGovtAppointments.push({
        dubaiAppointmentStartDate : $rootScope.dataToSendToMembers.termStartDate,
        dubaiAppointmentEndDate : $rootScope.dataToSendToMembers.termEndDate,
        dubaiAppointmentPosition : $rootScope.positionValue,
        dubaiAppointmentBoard : $rootScope.dataToSendToMembers.boardFunction
      });
    }
    var userDetails = window.localStorage.getItem('userDetails');
    $scope.loggedUser = JSON.parse(userDetails);
    $scope.regex = '^(\\+971)([ ]2|[ ]3|[ ]4|[ ]6|[ ]7|[ ]9|[ ]50|[ ]51|[ ]52|[ ]53|[ ]54|[ ]55|[ ]56|[ ]57|[ ]58|[ ]59)[ ][0-9]{7}$';//'^(\\+971)(2|3|4|6|7|9|50|55)\\d{7}$';
    $scope.showReadingDropdown = true;
    $scope.showWritingDropdown = true;
    $scope.showSpeakingDropdown = true;
    $scope.showLanguageDropdown = false;
    $scope.languageInForm = {};
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    };
    $scope.uploadedDocuments = [];
    $scope.supportingDocuments = [];
    $scope.data = {};
    $scope.experienceInForm = {};
    $scope.data.personalInformation = {
      'picture': {},
      'passportAttachment': {},
      'cvAttachment': {}
    };
    $scope.data.education = [];
    $scope.data.training = [];
    $scope.data.experience = [];
    $scope.data.research = [];
    $scope.data.language = [];
    $scope.data.skills = [];
    if ($scope.data.dubaiGovtAppointments == undefined) {
      $scope.data.dubaiGovtAppointments = [];
    }
    $scope.data.nonGovtAppointments = [];
    $scope.additionalData = {};
    $scope.setMode();
    $scope.educationInForm = {};
    $scope.trainingInForm = {};
    $scope.researchInForm = {};
    $scope.skillsInForm = {};
    $scope.dubaiGovtAppointmentInForm = {};
    angular.element(document).ready(function() {
      angular.element('.phone-num').on('keyup', function(event) {
        if (event.keyCode != 8 && event.keyCode != 46) { //backspace and delete key
          if(event.target.value.length == 6 && (event.target.value[5] == 2 || event.target.value[5] == 3 || event.target.value[5] == 4 || event.target.value[5] == 6 || event.target.value[5] == 7 || event.target.value[5] == 9)) {
            event.target.value = event.target.value.concat(" ");
            return;
          }
          if(event.target.value.length == 4 || (event.target.value[6] != ' ' && event.target.value.length == 7)) {
            event.target.value = event.target.value.concat(" ");
          }
        }
      });
    });
  }

  $scope.setMode = function() {
    switch($rootScope.memberMode){
      case "view":
        $scope.formSubmittedSuccessfully = true;
        $scope.hideSuccessMessage = true;
        $scope.viewMemberMode();
        break;

      case "create":
        $scope.formSubmittedSuccessfully = false;
        break;

      case "modify":
        $scope.viewMemberMode();
        $scope.formSubmittedSuccessfully = false;
        break;
    }
  };

  $scope.openFileInput = function (id) {
    if(typeof id === 'number') {
      angular.element('#input-'+id).trigger('click');
    } else {
      $timeout(function(){
        angular.element('#'+id).trigger('click');
      });
    }
  };

  $scope.deleteFileFromUpload = function (index) {
    $scope.supportingDocuments.splice(index, 1);
  };

  $scope.onSubmit = function(evt) {
    if (!$rootScope.loading) {
      $rootScope.loading = true;
      var uploadedDocs = $scope.supportingDocuments.concat($scope.documentsToSendToUpload);
      var promise = memberService.convertFile(uploadedDocs).then(function(fileArray) {
        var data = $scope.data;
        data.uploadedDocuments = [];
        for (var i =0; i<fileArray.length;i++) {
          if (fileArray[i]['name'] === 'picture') {
            data.personalInformation.picture = fileArray[i];
          } else if (fileArray[i]['name'] === 'cvAttachment') {
            data.personalInformation.cvAttachment = fileArray[i];
          } else if (fileArray[i]['name'] === 'passportAttachment') {
            data.personalInformation.passportAttachment = fileArray[i];
          } else {
            data.uploadedDocuments.push(fileArray[i]);
          }
        }
        //data.uploadedDocuments = fileArray;
        data = JSON.stringify(data);
        if ($scope.data.userId) {
          memberService.updateMember(data).success(function(data, status, headers, config) {
            $scope.formSubmittedSuccessfully = true;
            $scope.hideButtons = true;
            $rootScope.loading = false;
            $('html,body').animate({
              scrollTop: $('.alert-success').offset().top - 200
            }, 'slow');
          }).error(function(error) {
            if (error.code === '1001') {
              $scope.error = 'ERR_CODE_1001';
            } else {
              $scope.error = 'ERR_CODE_1002';
            }
            $('html,body').animate({
              scrollTop: $('.alert-danger').offset().top - 200
            }, 'slow');
            $rootScope.loading = false;
            $scope.error = null;
          });
        } else {
          memberService.createMember(data).success(function(data, status, headers, config) {
            $scope.formSubmittedSuccessfully = true;
            $scope.hideButtons = true;
            $rootScope.loading = false;
            $('html,body').animate({
              scrollTop: $('.alert-success').offset().top - 200
            }, 'slow');
          }).error(function(error) {
            if (error.code === '1001') {
              $scope.error = 'ERR_CODE_1001';
            } else {
              $scope.error = 'ERR_CODE_1000';
            }
            $('html,body').animate({
              scrollTop: $('.alert-danger').offset().top - 200
            }, 'slow');
            $rootScope.loading = false;
          });
        }
      });
    }
  };

  $scope.readingLanguageChange = function(){
    if ($scope.showReadingDropdown == false){
      $scope.showReadingDropdown = true;
    } else {
      $scope.showReadingDropdown = false;
    }
  }

  $scope.writingLanguageChange = function(){
    if ($scope.showWritingDropdown == false) {
      $scope.showWritingDropdown = true;
    } else {
      $scope.showWritingDropdown = false;
    }
  }

  $scope.speakingLanguageChange = function(){
    if ($scope.showSpeakingDropdown == false) {
      $scope.showSpeakingDropdown = true;
    } else {
      $scope.showSpeakingDropdown = false;
    }
  }

  $scope.sendLanguageFormData = function(){
    $scope.showLanguageTable = true;
    if ($scope.languageInForm.otherLanguage != undefined) {
      $scope.languageInForm.language = $scope.languageInForm.otherLanguage;
    }
    $scope.data.language.push($scope.languageInForm);
    $scope.languageInForm = {};
    $scope.showReadingDropdown = true;
    $scope.showWritingDropdown = true;
    $scope.showSpeakingDropdown = true;
  };

  $scope.sendFormData = function(){
    $scope.showExperienceTable = true;
    $scope.experienceInForm.countryName = angular.element('#experienceCountryForm option:selected').text();
    $scope.data.experience.push($scope.experienceInForm);
    $scope.experienceInForm = {};
  };

  $scope.uploadDocuments = function() {
    angular.element("#uploadDoc").trigger('click');
  };

  $scope.deleteRow = function (index, tableName) {
    if (tableName == 'skills') {
      $scope.skillset.unshift($scope.data[tableName][index].skillArea)
    }
    $scope.data[tableName].splice(index, 1);
  };

  $scope.formSubmitted = function() {
      $scope.isFormSubmitted = true;
      $scope.onSubmit();
  };

  $scope.viewMemberMode = function () {
    var id = memberService.getCurrentMember();
    $rootScope.loading = true;
    memberService.getMemberData(id).then(function(data) {
      $scope.data = data;
      if ($scope.data.education === undefined) {
        $scope.data.education = [];
      }
      if ($scope.data.training === undefined) {
        $scope.data.training = [];
      }
      if ($scope.data.experience === undefined) {
        $scope.data.experience = [];
      }
      if ($scope.data.research === undefined) {
        $scope.data.research = [];
      }
      if ($scope.data.language === undefined) {
        $scope.data.language = [];
      }
      if ($scope.data.skills === undefined) {
        $scope.data.skills = [];
      }
      if ($scope.data.dubaiGovtAppointments === undefined) {
        $scope.data.dubaiGovtAppointments = [];
      }
      if ($scope.data.nonGovtAppointments === undefined) {
        $scope.data.nonGovtAppointments = [];
      }
      
      $scope.data.role = data.roleId;
      $scope.data.personalInformation.mobileNumber = $scope.formatPhone($scope.data.personalInformation.mobileNumber);
      $scope.data.personalInformation.alternateNumber = $scope.formatPhone($scope.data.personalInformation.alternateNumber);
      $scope.alreadyUploadedDocuments = data.uploadedDocuments;
      $rootScope.loading = false;
    }, function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.formatPhone = function (val) {
    if(val) {
      var res = "";
      if(val.length == 12) {
        res = val.substr(0,4) + " " + val.substr(4,1) + " " + val.substr(5,7);
      } else if(val.length == 13) {
        res = val.substr(0,4) + " " + val.substr(4,2) + " " + val.substr(6,7);
      }
      return res;
    }
  };

  $scope.onExitClick = function () {
    $scope.navigateTo('viewDatabankMembers');
  };

  $scope.getSelectedValueMember = function(selectedValuesMember){
    if (selectedValuesMember == 'others') {
      $scope.flagOtherMember = true;
    } else{
      $scope.flagOtherMember  = false;
    }
  };

   $scope.getSelectedValueSkill = function(selectedValueSkill){
    $scope.selectedValue = selectedValueSkill;
    if (selectedValueSkill == 'Other') {
      $scope.flagOtherSkill = true;
    } else{
      $scope.flagOtherSkill  = false;
    }
  };

   $scope.getSelectedValuePosition = function(selectedValuePosition){
    if (selectedValuePosition == 'Other') {
      $scope.flagOtherGovtAppoint = true;
    } else{
      $scope.flagOtherGovtAppoint  = false;
    }
  };

  $scope.languageSelected = function(){
    if ($scope.languageInForm.language == "Other") {
      $scope.showLanguageDropdown = true;
    } else {
      $scope.showLanguageDropdown = false;
    }
  };

  $scope.sendEducationFormData = function() {
    $scope.educationInForm.countryName = angular.element('#educationCountry option:selected').text();
    $scope.data.education.push($scope.educationInForm);
    $scope.educationInForm = {};
  };

  $scope.sendTrainingFormData = function() {
    $scope.data.training.push($scope.trainingInForm);
    $scope.trainingInForm = {};
  };

  $scope.sendResearchFormData = function() {
    $scope.data.research.push($scope.researchInForm);
    $scope.researchInForm = {};
  };

  $scope.sendSkillsFormData = function() {
    if($scope.selectedValue != "Other"){
      var position = $scope.skillset.indexOf($scope.selectedValue);
      $scope.skillset.splice(position, 1);
    }
    if($scope.flagOtherSkill){
      $scope.skillsInForm.skillArea = $scope.skillsInForm.skillOther;
    }
    $scope.data.skills.push($scope.skillsInForm);
    $scope.skillsInForm = {};
  };

  $scope.skillsList = function() {
    $scope.skillset = [
    'Strategic Planning', 
    'Corporate Governance',
    'Sectorial Governance',
    'Financial Management',
    'Human Resources Management',
    'Policy Development',
    'Performance Management',
    'Project Management',
    'Investment',
    'Risk Management',
    'Other'
    ];
  };

  $scope.sendGovtFormData = function() {
    if($scope.flagOtherGovtAppoint){
      $scope.dubaiGovtAppointmentInForm.dubaiAppointmentPosition = $scope.dubaiGovtAppointmentInForm.dubaiGovtAppointOther;
    }
    $scope.data.dubaiGovtAppointments.push($scope.dubaiGovtAppointmentInForm);
    $scope.dubaiGovtAppointmentInForm = {};
  };

  $scope.sendNoGovtFormData = function() {
    $scope.data.nonGovtAppointments.push($scope.dubaiGovtAppointmentInForm);
    $scope.dubaiGovtAppointmentInForm = {};
  };

  $scope.previewFile = function(){
    var preview = document.querySelector('.uploadedImageForPreview'); //selects the query named img
    var file    = document.querySelector('input[type=file]').files[0]; //sames as here
    var reader  = new FileReader();
    reader.onloadend = function () {
      preview.src = reader.result;
    }
    if (file) {
      reader.readAsDataURL(file); //reads the data as a URL
    } else {
      preview.src = "";
    }
    var temp = {
      name: 'picture',
      type: 'File',
      file: $scope.data.personalInformation.profilePicture,
      comment: ''
    }
    $scope.documentsToSendToUpload.push(temp);
  };

  $scope.uploadingPassport = function (){
    var temp = {
      name: 'passportAttachment',
      type: 'File',
      file: $scope.data.personalInformation.passportAttachment,
      comment: ''
    }
    $scope.documentsToSendToUpload.push(temp);
  };

  $scope.uploadingCVAttachment = function() {
    var temp = {
      name: 'cvAttachment',
      type: 'File',
      file: $scope.data.personalInformation.cvAttachment,
      comment: ''
    }
    $scope.documentsToSendToUpload.push(temp);
  };

  $scope.getRoles = function (){
    memberService.getRoles().success(function(data, status, headers, config) {
      $scope.roles = data;
    }).error(function(error) {
      console.log(error);
    });
  };

  $scope.getCountries = function() {
    memberService.getCountries().success(function(data, status, headers, config) {
      $scope.countries = data;
    }).error(function(error) {
      console.log(error);
    });
  };

  $scope.onDocumentUploadSave = function () {
    $scope.supportingDocuments.push($scope.tempDocument);
    $('#documentModalBoard').modal('hide');
    $scope.tempDocument = {
      name: '',
      type: 'File',
      file: '',
      comment: ''
    }
  };
  
  $scope.init();
}]);
