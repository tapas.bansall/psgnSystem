var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('viewCommitteeController', ['$scope', '$rootScope', '$http', 'committeeService', function($scope, $rootScope, $http, committeeService) {
  $scope.init = function() {
    $scope.getRequests();
    // $scope.getStatus()
    $rootScope.actionPerformedIsNotCreate = false;
    $rootScope.modifyingEntity = false;
    $scope.searchData = {
      entityName: '',
      typology: ''
    }
    $scope.columnCount = 3;
    $scope.rowCount = 3;
    $scope.searchInitiated = false;
    $scope.entityData = [];
    $scope.dataOfEntity = [];
  };
  
  $scope.getRows = function() {
    if ($scope.data.length > 0) {
      return new Array(Math.ceil(($scope.data.length) / $scope.columnCount))
    }
  }

  $scope.getColumns = function() {
    return new Array($scope.columnCount)
  };

  $scope.filterByEntityName = function(request) {
    if ($scope.searchData && $scope.searchData.entityName && $scope.searchData.entityName.length > 0) {
      var entityAR = request.brandednameAR.toLowerCase();
      var entityEN = request.brandednameEN.toLowerCase();
      var filter = $scope.searchData.entityName.toLowerCase();
      return ((entityAR.indexOf(filter) !== -1) || (entityEN.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };


  $scope.searchEntity = function() {
    if ($scope.searchInitiated == false) {
      $scope.dataOfEntity = $.extend(true, [], $scope.data);
    }
    console.log($scope.data, "<-data")
    $scope.entityData = $.extend(true, [], $scope.dataOfEntity);
    if ($scope.searchInitiated == true) {
      $scope.entityData = $scope.dataOfEntity;
    }
    if ($scope.searchData.entityName != undefined && $scope.searchData.entityName.length > 0) {
      $scope.searchInitiated = true;
      var filter = $scope.searchData.entityName.toLowerCase();
      var foundArray = $scope.entityData.map(function(e) {
        return (e.brandednameAR.toLowerCase()).indexOf(filter);
      });
      for (var i = 0; i < foundArray.length; i++) {
        if (foundArray[i] == -1) {
          $scope.entityData.splice(i, 1);
          foundArray.splice(i, 1);
          i--;
        }
      }
      $scope.data = $scope.entityData;
      
    } else {
      console.log("not inside if")
      //$scope.searchInitiated = false;
      $scope.data = $scope.entityData;
    }
    return $scope.data;
  };


  $scope.getRequests = function() {
    $rootScope.loading = true;
    committeeService.getAllBoardCommittee().success(function(data) {
      $scope.data = data;
      console.log("response from get all board and committ",$scope.data)
      $rootScope.loading = false;
      $scope.dataOfEntity = $.extend(true, [], $scope.data);
      $scope.getRows();
      $scope.getColumns();
      $scope.selectedForDeactivation = {};
    }).error(function(error) {
      $rootScope.loading = false;
    });
  };

  // $scope.getStatus = function(){
  //   $rootScope.loading = true;
  //   committeeService.getStatusOfRequest().success(function(data){
  //     $scope.data = data
  //   })
  // }

  $scope.filterByEntityName = function(request) {
    if ($scope.searchData && $scope.searchData.entityName && $scope.searchData.entityName.length > 0) {
      var entityAR = request.brandednameAR.toLowerCase();
      var entityEN = request.brandednameEN.toLowerCase();
      var filter = $scope.searchData.entityName.toLowerCase();
      return ((entityAR.indexOf(filter) !== -1) || (entityEN.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };

  $scope.filterByTypology = function(request) {
    if ($scope.searchData && $scope.searchData.typology && $scope.searchData.typology.length > 0) {
      var le = request.typology;
      var filter = $scope.searchData.typology;
      le = le.toString();
      return ((le.indexOf(filter) !== -1) || (le.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };

  //$rootScope.buttonStatus = function(exit, save, del, submit, approve, reject, complete, proceed, coordinatorCommentSection, approverCommentSection){
  $scope.viewCommitteeClick = function() {
   
    committeeService.saveBoardAndCommittee($rootScope.radioEnabledEntity.boardCommitteeId);
    $rootScope.committeeMode = 'view';
    $scope.navigateTo('createCommitee');
    $rootScope.buttonStatus(false, false, false, false, false, true, false, false, false, false, false);
  };

  $scope.modifyCommitteeClick = function(id) {

    $rootScope.loading = true;
    committeeService.saveBoardAndCommittee($rootScope.radioEnabledEntity.boardCommitteeId);
    $rootScope.committeeMode = 'modify';
    $scope.navigateTo('createCommitee');
    $rootScope.buttonStatus(true, true, false, true, false, false, false, false, false, false, false);
    
  };

  $scope.renewCommitteeClick = function(id) {
    $rootScope.disableFields = false;
    $scope.data.nameEN = $rootScope.radioEnabledEntity.nameEN;
    $scope.data.nameAR = $rootScope.radioEnabledEntity.nameAR;
    $rootScope.hhApprovalCommentsDisabled = true;
    $rootScope.accordianStatus(false);
    $rootScope.coordinatorCommentsDisabled = false;
    $rootScope.buttonStatus(true, true, false, true, false, false, false, false, false, false, false);
    $rootScope.formDisabled = false;
    committeeService.saveCurrentCommittee(id);
    //$scope.navigateTo('createCommitee');
  };

  $scope.evaluateCommitteeClick = function(id) {
    $rootScope.disableFields = false;
    $rootScope.hhApprovalCommentsDisabled = true;
    $rootScope.accordianStatus(false);
    $rootScope.coordinatorCommentsDisabled = false;
    $rootScope.buttonStatus(true, true, false, true, false, false, false, false, false, false, true);
    $rootScope.formDisabled = false;
    committeeService.saveCurrentCommittee(id);
    $scope.navigateTo('createCommitee');
  };

  $scope.setSelectedEntity = function(index) {
    $scope.selectedForDeactivation = $scope.data[index];
  };

  $scope.openModalFileInput = function(id) {
    $('#' + id).trigger('click');
  };

  $scope.onDeactivateSave = function() {
    var data = $scope.selectedForDeactivation;
  };

  $scope.renewCommittee = function() {
    $rootScope.loading = true;
    $scope.data = data;
    // data.requestType = $rootScope.committeeMode;
    data.committeeId = $rootScope.radioEnabledEntity.boardCommitteeId;
    data.extension = $scope.data.extension;
    committeeService.renewBoardAndCommittee(data).success(function(data) {  
      
      $rootScope.loading = false;
      //$scope.navigateTo('createCommitee');
    }).error(function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.init();
}]);