var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('approvalSummaryController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    //something here
    // $scope.getReviewCount();
    $scope.approverReview = {
      overallComments: ''
    };
    $scope.viewableComments = {
      legislationComments: '',
      structureComments: '',
      functionalDescriptionComments: '',
      employeeComments: '',
      expenseComments: '',
      documentComments: ''
    };
  };

  $scope.getReviewStatus = function () {
    $http.get('http://localhost:8809/path-to-api', {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.onSubmit = function () {

  };

  $scope.setViewableComments = function (comments) {
    $scope.viewableComments = comments;
    
  };

  $scope.viewRequest = function () {
    window.location.hash = '/viewRequest';
  }

  $scope.init();
}]);