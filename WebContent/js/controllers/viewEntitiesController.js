var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('viewEntitiesController', ['$scope', '$rootScope', '$http', 'orderByFilter', 'entityService', 'memberService', function($scope, $rootScope, $http, orderBy, entityService, memberService) {
  $rootScope.loading = true;
  $rootScope.showButtonBasedOnRadio = false;
  $scope.init = function() {
    $scope.getRequests();
    $rootScope.actionPerformedIsNotCreate = false;
    $rootScope.modifyingEntity = false;
    $scope.searchData = {
      entityName: '',
      typology: ''
    };
    $scope.columnCount = 3;
    $scope.rowCount = 3;
    $scope.searchInitiated = false;
    $scope.entityData = [];
    $scope.dataOfEntity = [];
    $scope.uploadedDocuments = [];
  };

  $scope.getRequests = function() {
    $rootScope.loading = true;
    entityService.getAllEntities().success(function(data) {
      if ($rootScope.application.language === 'en') {
        data = orderBy(data, 'brandednameEN');
      } else {
        data = orderBy(data, 'brandednameAR');
      }
      $scope.data = data;
      $rootScope.loading = false;
      $scope.originalData = data;
      $scope.numberOfRows = $scope.getRows();
    }).error(function(error) {
      $rootScope.loading = false;
    });
  };

  $scope.getRows = function() {
    if (!$rootScope.loading && $scope.data.length > 0) {
      return new Array(Math.ceil(($scope.data.length) / $scope.columnCount))
    }
  }

  $scope.getColumns = function() {
    return new Array($scope.columnCount)
  }

  $scope.filterByEntityName = function(request) {
    if ($scope.searchData && $scope.searchData.entityName && $scope.searchData.entityName.length > 0) {
      var entityAR = request.brandednameAR.toLowerCase();
      var entityEN = request.brandednameEN.toLowerCase();
      var filter = $scope.searchData.entityName.toLowerCase();
      return ((entityAR.indexOf(filter) !== -1) || (entityEN.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };


  $scope.searchEntity = function() {
    if ($scope.searchData.entityName.length > 0) {
      $scope.data = [];
      for (var i=0;i<$scope.originalData.length;i++) {
        var name = $scope.originalData[i]['brandednameEN'];
        if (name.toLowerCase().indexOf($scope.searchData.entityName.toLowerCase()) > -1) {
          $scope.data.push($scope.originalData[i]);
        }
      }
    } else {
      $scope.data = $scope.originalData;
    }
    $scope.numberOfRows = $scope.getRows();
  };

  $scope.filterByTypology = function(request) {
    if ($scope.searchData && $scope.searchData.typology && $scope.searchData.typology.length > 0) {
      var le = request.typology;
      var filter = $scope.searchData.typology;
      le = le.toString();
      return ((le.indexOf(filter) !== -1) || (le.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };

  $scope.modifyEntityClick = function() {
    $rootScope.actionPerformedIsNotCreate = true;
    $rootScope.modifyingEntity = true;
    entityService.saveCurrentEntity($rootScope.radioEnabledEntity.entityId);
    $rootScope.entityMode = 'modify';
    $scope.navigateTo('createRequest');
  };

  $scope.viewEntityClick = function() {
    entityService.saveCurrentEntity($rootScope.radioEnabledEntity.entityId);
    $rootScope.actionPerformedIsNotCreate = 'true';
    $rootScope.entityMode = 'view';
    $scope.navigateTo('createRequest');
  };

  $scope.setSelectedMember = function() {
    $scope.selectedForDeactivation = $rootScope.radioEnabledEntity;
  };

  $scope.openModalFileInput = function(id) {
    window.setTimeout(function() {
      $('#' + id).trigger('click');
    }, 500);
  };

  $scope.onDeactivateSave = function() {
    $rootScope.loading = true;
    if ($scope.selectedForDeactivation.file) {
      var docs = [{
        name: $scope.selectedForDeactivation.file[0].name,
        file: $scope.selectedForDeactivation.file,
        type: 'File',
        comment: ''
      }];
    } else {
      var docs = [];
    }
    var promise = memberService.convertFile(docs).then(function(fileArray) {
      var data = $scope.selectedForDeactivation;
      data.entityID = data.entityId;
      data.requestType = 'Deactivate';
      data.status = 'submitted';
      data.userId = $rootScope.loggedUser.userId;
      data.creationDate = new Date();
      data.uploadedDocuments = fileArray;
      data = JSON.stringify(data);
      $http.post('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/createEntity', data, {
        headers: {
          'Content-Type': 'application/json'
        },
        transformRequest: angular.identity
      }).success(function(data, status, headers, config) {
        $scope.callType = 'deactivate';
        $rootScope.loading = false;
        $('#entityDeactivationModal').modal('hide');
      }).error(function(error) {
        $rootScope.loading = false;
      });
    });
  };

  $scope.init();
}]);