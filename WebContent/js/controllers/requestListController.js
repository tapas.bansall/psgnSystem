var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('requestListController', ['$scope', '$rootScope', '$http', 'entityService', function($scope, $rootScope, $http, entityService) {
  $scope.init = function() {
    $scope.pages = [];
    $scope.limit = 5;
    $rootScope.isRequestCompleted = false;
    $rootScope.isStatusPending = false;
    $rootScope.actionPerformedIsNotCreate = false;
    $rootScope.modifyingEntity = false;
    $rootScope.actionPerformedIsComplete = false;
    $rootScope.showSubmitButton = false;
    $scope.getRequests();
  };

  $scope.getRequests = function() {
    $rootScope.loading = true;
    $http.get('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/reviewList?userId=' + $rootScope.loggedUser.userId, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data.Output;
      $rootScope.loading = false;
    }).error(function (error) {
      $rootScope.loading = false;
    });
  };

  $scope.viewEntityClick = function(record) {
    $rootScope.actionPerformedIsNotCreate = true;
    entityService.saveCurrentEntity(record.entityId);
    if (record.status == "To be modified") {
      $rootScope.entityMode = 'modify';
      $rootScope.modifyingEntity = true;
    } else if (record.status == "Pending") {
      $rootScope.entityMode = 'view';
      $rootScope.isStatusNotPending = true;
    } else if (record.status == "To be completed") {
      $rootScope.entityMode = 'view';
      $rootScope.isStatusNotPending = true;
      $rootScope.actionPerformedIsComplete = true;
    } else {
      $rootScope.entityMode = 'view';
    }
    $scope.navigateTo('createRequest');
  };

  $scope.modifyEntityClick = function(record) {
    $rootScope.actionPerformedIsNotCreate = true;
    $rootScope.entityMode = 'modify';
    $rootScope.modifyingEntity = true;
    entityService.saveCurrentEntity(record.entityId);
    if (record.status == "Pending") {
      $rootScope.entityMode = 'addReview';
    } else {
      $rootScope.entityMode = 'view';
    }
    $scope.navigateTo('createRequest');
  };

  $scope.openRequest = function() {
    window.location.hash = '/viewRequest';
  };

  $scope.init();
}]);