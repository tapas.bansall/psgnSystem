var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('createRequestController', ['$scope', '$rootScope', '$http', '$timeout', 'entityService', 'memberService', function ($scope, $rootScope, $http, $timeout, entityService, memberService) {
  $scope.init = function () {
    $scope.flagOther  = false;
    var fundingSourceValues = [];
    $scope.data = {
      boardInclusion: '',
      typology: '',
      affiliation: '',
      additionalMandate: []
    };
    $scope.tempDocument = {
      name: '',
      type: 'File',
      file: '',
      comment: ''
    };
    $scope.entities = [];
    $scope.status = {
      0 : "pre-draft",
      1 : "drafted",
      2 : "submitted",
      3 : "approved",
      4 : "rejected",
      5 : "legislation-signed",
      6 : "complete"
    };

    $scope.getDirectors();
    $scope.currentStatus = $scope.status['0'];
    $scope.uploadedDocuments = [];
    $scope.summaryInformation = {};
    $scope.governanceFunctions = {};
    $scope.governingLegistlation = {};
    $scope.hideSuccessMessage = false;
    $rootScope.showReviewTab = false;

    angular.element(document).ready(function() {
      window.setTimeout(function() {
        $('.multiselect-ui').multiselect({
          includeSelectAllOption: true
        });
      }, 1000);
    });
    $scope.setMode();
  };

  $scope.openFileInput = function (id) {
    if(typeof id === 'number') {
      angular.element('#input-'+id).trigger('click');
    } else {
      $timeout(function(){
        angular.element('#'+id).trigger('click');
      });
    }
  };

  $scope.onSubmit = function (evt) {
    if (!$rootScope.loading) {
      $rootScope.loading = true;
      $scope.data.active = 1;
      $scope.data.userId = $rootScope.loggedUser.userId;
      $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
      if ($rootScope.userIsCoordinator) {
        $scope.data.status = "submitted";
      } else if($rootScope.userIsSLCCoordinator) {
        $scope.data.status = "hhApproved";
      }
      memberService.convertFile($scope.uploadedDocuments).then(function(fileArray) {
        var data = $scope.data;
        data.requestType = $scope.requestType;
        data.uploadDocuments = fileArray;
        data = JSON.stringify(data);
        $http.post('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/createEntity', data, {
            headers: { 'Content-Type':'application/json'},
            transformRequest: angular.identity
        }).success(function (data, status, headers, config) {
          $scope.formSubmittedSuccessfully = true;
          $scope.hideButtons = true;
          $rootScope.loading = false;
          $('html,body').animate({
            scrollTop: $('.alert-success').offset().top - 200
          }, 'slow');
          $scope.error = false;
        }).error(function (error) {
          $('html,body').animate({
            scrollTop: $('.alert-danger').offset().top - 200
          }, 'slow');
          $scope.error = true;
          $rootScope.loading = false;
        });
      });
    }
  };

  $scope.approveRequest = function() {
    var data = {};
    data.status = 'approved';
    data.justification = $scope.data.justification;
    data.taskId = '123';
    $http.post('http://localhost:8080/submit', data, {
        headers: { 'Content-Type': undefined},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      console.log('data posted');
      $scope.formSubmittedSuccessfully = true;
      $scope.hideButtons = true;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.rejectRequest = function() {
    var data = {};
    data.status = 'rejected';
    data.justification = $scope.data.justification;
    data.taskId = '123';
    $http.post('http://localhost:8080/submit', data, {
        headers: { 'Content-Type': undefined},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      console.log('data posted');
      $scope.formSubmittedSuccessfully = true;
      $scope.hideButtons = true;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.completeRequest = function() {
    var data = {};
    data.status = 'completed';
    data.taskId = '123';
    $http.post('http://localhost:8080/submit', data, {
        headers: { 'Content-Type': undefined},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      console.log('data posted');
      $scope.formSubmittedSuccessfully = true;
      $scope.hideButtons = true;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.saveAsDraft = function() {
    $rootScope.loading = true;
    $scope.data.userId = $rootScope.loggedUser.userId;
    $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
    $scope.data.status = "draft";
    memberService.convertFile($scope.uploadedDocuments).then(function(fileArray) {
      var data = $scope.data;
      data.requestType = $scope.requestType;
      data.uploadDocuments = fileArray;
      data = JSON.stringify(data);
      $rootScope.dataToSendToMembersFromCreateRequest = $scope.data;
      $http.post('http://172.16.8.248:5555/rest/PSGN_Entity/restServices/createEntity', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
      }).success(function (data, status, headers, config) {
        $scope.formSubmittedSuccessfully = true;
        $scope.hideButtons = true;
        $rootScope.loading = false;
        $('html,body').animate({
          scrollTop: $('.alert-success').offset().top - 200
        }, 'slow');
      }).error(function (error) {
        $scope.error = true;
        $rootScope.loading = false;
        $('html,body').animate({
          scrollTop: $('.alert-danger').offset().top - 200
        }, 'slow');
      });
    });
  };

  $scope.onDocumentUploadSave = function () {
    $scope.uploadedDocuments.push($scope.tempDocument);
    $('#documentModalBoard').modal('hide');
    $scope.tempDocument = {
      name: '',
      type: 'File',
      file: '',
      comment: ''
    }
  };

  $scope.uploadDocuments = function() {
    angular.element("#uploadDoc").trigger('click');
  };

  $scope.deleteFileFromUpload = function (index) {
    $scope.uploadedDocuments.splice(index, 1);
  };

  $scope.formSubmitted = function () {
    $scope.isFormSubmitted = true;
    if ($scope.createRequest.$valid) {
      $scope.onSubmit();
    } else {
      alert('Form is invalid');
    }
  };

  $scope.viewEntityMode = function () {
    var id = entityService.getCurrentEntity();
    entityService.getEntityData(id).then(function(data) {
      $scope.data = data;
      if ($rootScope.entityMode === 'modify') {
        $scope.requestType = 'Modify';
      } else {
        $scope.requestType = 'Create';
      }
    }, function(error) {
      console.log(error);
    });
  };

  $scope.setMode = function() {
    switch($rootScope.entityMode){
      case "view":
        $scope.formSubmittedSuccessfully = true;
        $scope.hideSuccessMessage = true;
        $scope.viewEntityMode();
        break;

      case "create":
        $scope.formSubmittedSuccessfully = false;
        $scope.requestType = "Create";
        break;

      case "modify":
        $scope.formSubmittedSuccessfully = false;
        $scope.formModifiedSuccessfully = false;
        $scope.requestType = "Modify";
        $scope.viewEntityMode();
        break;

      case "addReview":
        $rootScope.showReviewTab = true;
        $scope.formSubmittedSuccessfully = true;
        $scope.hideSuccessMessage = true;
        $scope.viewEntityMode();
        break;
    }

    $rootScope.goPreviousPage = function(){
      window.history.back();
    };
  }

  $scope.getSelectedValue = function(fundingSourceValues){
    angular.forEach(fundingSourceValues, function(index){
      if(fundingSourceValues.indexOf('Other') >= 0)
      {
        $scope.flagOther = true;
      }
      else {
        $scope.flagOther  = false;
      }
    });
  };

  // $scope.getEntity = function () {
  //   if($rootScope.entityMode !== "new"){
  //   	$rootScope.loading = true;
	 //    entityService.getEntity($rootScope.radioEnabledEntity.entityId).success(function(data) {
	 //      $scope.data = data;
	 //      $rootScope.loading = false;
	 //    }).error(function(error) {
	 //      $rootScope.loading = false;
	 //    });
  //   }
  // };

  $scope.getDirectors = function () {
    $http.get('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/getDataBankMembers?roleId=2', {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.directorList = data.res;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.selectAdditionalMandate = function(){
    $('#additionalMandate').multiselect('select', [$scope.data.typology]);
  }

  $scope.init();
}]);
