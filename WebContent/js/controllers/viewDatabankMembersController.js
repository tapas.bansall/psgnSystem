var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('viewDatabankMembersController', ['$scope', '$rootScope', '$http', 'orderByFilter', 'memberService', function($scope, $rootScope, $http, orderBy, memberService) {
  $scope.init = function() {
    $scope.getRequests();
    $rootScope.showButtonBasedOnRadio = false;
    $scope.searchData = {
      memberName: '',
      memberEmail: ''
    }
    $scope.columnCount = 3;
    $scope.rowCount = 3;
    $scope.searchInitiated = false;
    $scope.entityData = [];
    $scope.dataOfEntity = [];
    $scope.uploadedDocuments = [];
    $scope.numberOfRows = 0;
  };

  $scope.getRows = function() {
    return new Array(Math.ceil(($scope.data.length) / $scope.columnCount))
  };

  $scope.searchEntity = function () {
    if ($scope.searchData.entityName.length > 0) {
      $scope.data = [];
      for (var i=0;i<$scope.originalData.length;i++) {
        var name = $scope.originalData[i]['nameEN'];
        if (name.toLowerCase().indexOf($scope.searchData.entityName.toLowerCase()) > -1) {
          $scope.data.push($scope.originalData[i]);
        }
      }
    } else {
      $scope.data = $scope.originalData;
    }
    $scope.numberOfRows = $scope.getRows();
  };

  $scope.getColumns = function() {
    return new Array($scope.columnCount);
  };

  $scope.getRequests = function() {
    $rootScope.loading = true;
    $http.get('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/getDataBankMembers', {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      if ($rootScope.application.language === 'en') {
        data = orderBy(data.res, 'nameEN');
      } else {
        data = orderBy(data.res, 'nameAR');
      }
      $scope.data = data;
      $scope.originalData = data;
      $scope.numberOfRows = $scope.getRows();
      $rootScope.loading = false;
    }).error(function (error) {
      console.log(error);
      $rootScope.loading = false;
    });
  };

  $scope.filterByMemberName = function(request) {
    if ($scope.searchData && $scope.searchData.memberName && $scope.searchData.memberName.length > 0) {
      var memberAR = request.nameAR.toLowerCase();
      var memberEN = request.nameEN.toLowerCase();
      var filter = $scope.searchData.memberName.toLowerCase();
      return ((memberAR.indexOf(filter) !== -1) || (memberEN.indexOf(filter) !== -1));
    } else {
      return true;
    }
  };

  $scope.filterByMemberEmail = function(request) {
    if ($scope.searchData && $scope.searchData.memberEmail && $scope.searchData.memberEmail.length > 0) {
      var me = request.emailAddress.toLowerCase();
      var filter = $scope.searchData.memberEmail.toLowerCase();
      return (me.indexOf(filter) > -1);
    } else {
      return true;
    }
  };

  $scope.viewMemberClick = function() {
    debugger;
    memberService.saveCurrentMember($rootScope.radioEnabledEntity.UserId);
    $rootScope.memberMode = 'view';
    $scope.navigateTo('createMember');
  };

  $scope.setSelectedModifyMember = function() {
    $scope.selectedForModify = $rootScope.radioEnabledEntity;
    $scope.selectedForModify.modifyDbmType = 'security_clearance';
  };

  $scope.setSelectedMember = function() {
    $scope.selectedForDeactivation = $rootScope.radioEnabledEntity;
  };

  $scope.onDbmModifySave = function(index) {
    $rootScope.loading = true;
    var data = $scope.selectedForModify;
    data.securityClearance = false;
    data = JSON.stringify(data);
    $http.post('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/modifyMemberForSecurityClearance', data, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.callType = 'sendForSecurity';
      $rootScope.loading = false;
      $('#memberModifyModal').modal('hide');
    }).error(function (error) {
      $rootScope.loading = false;
    });
  };

  $scope.onDbmUpdateProfileContinue = function() {
    memberService.saveCurrentMember($scope.radioEnabledEntity.UserId);
    $rootScope.memberMode = 'modify';
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open');
    $scope.navigateTo('createMember');
  };

  $scope.showCreateRequest = function () {
    $rootScope.memberMode = 'create';
    $scope.navigateTo('createMember');
  };

  $scope.onDbmAskMemberContinue = function() {
    $rootScope.loading = true;
    var data = $scope.selectedForModify;
    data.partialProfile = true;
    data = JSON.stringify(data);
    $http.post('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/markPartialProfile', data, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.callType = 'partialProfile';
      $rootScope.loading = false;
      $('#memberModifyModal').modal('hide');
    }).error(function (error) {
      $rootScope.loading = false;
    });
  };

  $scope.openModalFileInput = function(id) {
    window.setTimeout(function() {
      $('#' + id).trigger('click');  
    }, 500);
  };

  $scope.onDeactivateSave = function() {
    $rootScope.loading = true;
    if ($scope.selectedForDeactivation.file) {
      var docs = [{
        name: $scope.selectedForDeactivation.file[0].name,
        file: $scope.selectedForDeactivation.file,
        type: 'File',
        comment: ''
      }];
    } else {
      var docs = [];
    }
    var promise = memberService.convertFile(docs).then(function(fileArray) {
      var data = $scope.selectedForDeactivation;
      data.uploadedDocuments = fileArray;
      data = JSON.stringify(data);
      $http.post('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/deactivateDatabankMember', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
      }).success(function (data, status, headers, config) {
        $scope.callType = 'deactivate';
        $rootScope.loading = false;
        $('#memberDeactivationModal').modal('hide');
      }).error(function (error) {
        $rootScope.loading = false;
      });
    });
  };

  $scope.clearSecurityForMember = function(member) {
    $rootScope.loading = true;
    var data = $scope.selectedForModify;
    data.securityClearance = true;
    data = JSON.stringify(data);
    $http.post('http://172.16.8.248:5555/rest/PSGN_DatabankMembers/restServices/modifyMemberForSecurityClearance', data, {
      headers: { 'Content-Type': 'application/json'},
      transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.callType = 'sendForSecurity';
      $rootScope.loading = false;
      $('#memberModifyModal').modal('hide');
      $('#memberClearSecurityModal').modal('hide');
    }).error(function (error) {
      $rootScope.loading = false;
    });
  };

  $scope.init();
}]);