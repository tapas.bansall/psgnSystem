var psgnSystem = angular.module('psgnSystem');
psgnSystem.controller('requestMemberListController', ['$scope', '$rootScope', '$http', 'memberService', function ($scope, $rootScope, $http, memberService) {
  $scope.init = function () {
    $scope.getRequests();
  };

  $scope.getRequests = function () {
    // $http.get('http://localhost:8809/path-to-api', {
    //   headers: { 'Content-Type': 'application/json'},
    //   transformRequest: angular.identity
    // }).success(function (data, status, headers, config) {
    //   $scope.data = data;
    //   $scope.createPagination();
    //   $scope.handlePagination(1);
    // }).error(function (error) {
    //   console.log(error);
    // });
    $scope.data = [{
        memberId: 5,
        submissionType: 1,
        referenceNumber: 1,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 4,
        submissionType: 2,
        referenceNumber: 2,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 2,
        submissionType: 3,
        referenceNumber: 3,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 1,
        submissionType: 4,
        referenceNumber: 4,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 14,
        submissionType: 5,
        referenceNumber: 5,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 12,
        submissionType: 5,
        referenceNumber: 5,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 11,
        submissionType: 5,
        referenceNumber: 5,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 8,
        submissionType: 6,
        referenceNumber: 6,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      },
      {
        memberId: 7,
        submissionType: 7,
        referenceNumber: 7,
        nameAR: "ABC",
        nameEN: "XYZ",
        emailAddress: "abc@abc.com",
        date: 123,
        status: "Pending",
        comment: "Lorem ipsum"
      }];
  };

  $scope.openRequest = function (record) {
    memberService.saveCurrentMember(record.memberId);
    $rootScope.memberMode = 'view';
    $scope.navigateTo('createMember');
  };

  $scope.init();
}]);
