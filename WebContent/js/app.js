/*global angular */

/**
 * The main Account Management app module
 *
 * @type {angular.Module}
 */

//window.system = 'psgnsystem/';
window.system = './';
//window.system = '';
var psgnSystem = angular.module('psgnSystem', ['ngRoute', 'angularjsFileModel', 'pascalprecht.translate']);
psgnSystem.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
        controller: 'loginController',
        templateUrl: window.system + 'pages/login.html',
        header: false,
        footer: true,
        login: true
    }).when('/dashboard', {
        controller: 'dashboardController',
        templateUrl: window.system + 'pages/dashboard.html',
        header: true,
        footer: true
    }).when('/createRequest', {
        controller: 'createRequestController',
        templateUrl: window.system + 'pages/createRequest.html',
        header: true,
        footer: true
    }).when('/reviewlisting', {
        controller: 'reviewListingController',
        templateUrl: window.system + 'pages/reviewListing.html',
        header: true,
        footer: true
    }).when('/viewRequest', {
        controller: 'reviewController',
        templateUrl: window.system + 'pages/viewRequest.html',
        header: true,
        footer: true
    }).when('/approveRequest', {
        controller: 'approvalSummaryController',
        templateUrl: window.system + 'pages/approvalSummary.html',
        header: true,
        footer: true
    }).when('/createCommitee', {
        controller: 'commiteeController',
        templateUrl: window.system + 'pages/createCommitee.html',
        header: true,
        footer: true
    }).when('/createMember', {
        controller: 'memberController',
        templateUrl: window.system + 'pages/createMember.html',
        header: true,
        footer: true
    }).when('/requestList', {
        controller: 'requestListController',
        templateUrl: window.system + 'pages/requestList.html',
        header: true,
        footer: true
    }).when('/viewEntities', {
        controller: 'viewEntitiesController',
        templateUrl: window.system + 'pages/viewEntities.html',
        header: true,
        footer: true
    }).when('/searchEntities', {
        templateUrl: window.system + 'pages/searchEntities.html',
        header: true,
        footer: true
    }).when('/requestMemberList', {
        controller: 'requestMemberListController',
        templateUrl: window.system + 'pages/requestMemberList.html',
        header: true,
        footer: true
    }).when('/viewDatabankMembers', {
        controller: 'viewDatabankMembersController',
        templateUrl: window.system + 'pages/viewDatabankMembers.html',
        header: true,
        footer: true
    }).when('/statusOfRequestsBordCommitte', {
        controller: 'statusOfRequestsBoardCommitteeController',
        templateUrl: window.system + 'pages/statusOfRequestsBoardCommittee.html',
        header: true,
        footer: true
    }).when('/viewCommittee', {
        controller: 'viewCommitteeController',
        templateUrl: window.system + 'pages/viewCommittee.html',
        header: true,
        footer: true   
    }).when('/actionList', {
        controller: 'actionListController',
        templateUrl: window.system + 'pages/actionList.html',
        header: true,
        footer: true 
    }).when('/needToKnow', {
        controller: 'needToKnowController',
        templateUrl: window.system + 'pages/needToKnow.html',
        header: true,
        footer: true
    }).when('/publishCommunication',{
        controller: 'notificationController',
        templateUrl: window.system + 'pages/publishNotification.html',
        header: true,
        footer: true
    }).when('/needMoreSupport',{
        controller: 'needMoreSupportController',
        templateUrl: window.system + 'pages/needMoreSupport.html',
        header: true,
        footer: true
    }).otherwise({
        redirectTo: '/'
    });
    window.routeProvider = $routeProvider;
}]);

psgnSystem.config(['$httpProvider', function($httpProvider) {
    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }
    if (!$httpProvider.defaults.headers.post) {
      $httpProvider.defaults.headers.post = {};
    }
    if (!$httpProvider.defaults.headers.common) {
      $httpProvider.defaults.headers.common = {};
    }
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    $httpProvider.defaults.headers.common.Authorization = 'Basic VEVDVXNlcjpURUNTYWchMjM=';
  }]);
  


psgnSystem.run(function($rootScope, $location) {
  // register listener to watch route changes
  $rootScope.$on("$routeChangeStart", function(event, next, current) {
    if (next.$$route.originalPath !== '') {
      if ($rootScope.userLogged === undefined || !$rootScope.userLogged) {
        // no logged user, we should be going to #login
        if (!next.directAccess) {
          $location.path("/");
        }
      }
    }
    $rootScope.header = next.header;
    $rootScope.footer = next.footer;
    $rootScope.login = next.login;
  });

    $rootScope.buttonStatus = function(exit, save, del, submit, approve, reject, complete, proceed, coordinatorCommentSection, approverCommentSection, chairCommentSection){
        $rootScope.exitButton = exit;
        $rootScope.saveButton = save;
        $rootScope.deleteButton = del;
        $rootScope.submitButton = submit;
        $rootScope.approveButton = approve;
        $rootScope.rejectButton = reject;
        $rootScope.completeButton = complete;
        $rootScope.proceedButton = proceed;
        $rootScope.coordinatorCommentSection = coordinatorCommentSection;
        $rootScope.approverCommentSection = approverCommentSection;
        $rootScope.chairCommentSection = chairCommentSection;
    }

     $rootScope.accordianStatus = function(hhSupportingDoc){
        $rootScope.showHHapprovaldocuments = hhSupportingDoc;
    }

});

var setUpProject = function() {
  angular.element('.moduleContainer').each(function(index, moduleContainer) {
    angular.bootstrap(moduleContainer, angular.element(moduleContainer).data('ngModules'));
  });
}
$(document).ready(setUpProject);
